#ifndef INPUTLISTENER_H_INCLUDED
#define INPUTLISTENER_H_INCLUDED

#include<GL/glfw.h>
//TODO document
//TODO will need restructure to comply with glfw3
struct InputListener{

    InputListener();

    virtual void keyPressed(int key){}
    virtual void keyReleased(int key){}
    virtual void mousePressed(int button){}
    virtual void mouseReleased(int button){}
    virtual void mouseClicked(int button){}
    virtual void mouseMoved(int mouseX, int mouseY){}
    virtual void mouseDragged(int mouseX, int mouseY){}

    //declare static
    static InputListener *focus;

    static void keyChanged(int key, int action);

    static bool mouseDown;
    static int buttonDown;

    static void mouseButtonChanged(int button, int action);

    int mouseX, mouseY;
    static void mousePosChanged(int X, int Y);

    void SetFocus();



};

#endif // INPUTLISTENER_H_INCLUDED
