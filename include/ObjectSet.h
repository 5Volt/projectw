#ifndef OBJECTSET_H
#define OBJECTSET_H

#include<SRef.h>
#include<vector>
#include<GMath.h>
#include<map>
#include<list>
#include<algorithm>
#include<Object.h>

//TODO document

class ObjectSet
{
public:
    
    ///Iterator compatibility
    typedef SRef<Object> value_type;
    typedef SRef<Object>* pointer;
    typedef SRef<Object>& reference;
    typedef std::vector<value_type>::iterator iterator;
    ///METHODS
    ObjectSet();
    virtual ~ObjectSet();
    //iterator functions
    iterator begin();
    iterator end();
    
    //stl container methods
    void insert(const value_type &);
    
    reference operator[](size_t);
    void remove(const iterator unit);
    void remove(const value_type);
    
    SRef<Object> getObjectAt(const Object *pointer) const;
    
    //spatial hash methods
    std::list<value_type> getAdjacent(GMath::vec2 toPoint, double adjacency = 1.0)const;
    void updateHash();
    
    void setHashSize(double);
    
    
protected:
    //helper methods
    
    double hashSize;
    mutable std::map<int, std::map<int, std::list<value_type> > > hash;
    std::vector<value_type> myVect;
private:
};

#endif // OBJECTSET_H
