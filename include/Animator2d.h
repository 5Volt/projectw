#ifndef ANIMATOR2D_H
#define ANIMATOR2D_H

#include<GL/glfw.h>

#include<SRef.h>
#include<GMath.h>
#include<Animation2d.h>

class Animator2d
{
    public:
        Animator2d();
        Animator2d(SRef<Animation2d> newAnimation);

        virtual ~Animator2d();

        /**
         * Updates the time of the current animation. In order to get a moving animation,
         * update() must be called before each frame is drawn.
         * If multiple offsets are required, calling update for one set guarantees synchronisation
         * between all subsequent calls of getOffset() until another call of update() is made
         **/
        virtual void update();

        /**
         * Returns the offset for a particular element in a GObj2d at the current time(as last checked by update())
         * @param   index   The index of the element for which the offset is returned
         **/
        virtual const GMath::mat3 getOffset(size_t index);

        /**
         * Associates this animator with a new Animation, and resets it to the first frame.
         * @param   newAnimation    Reference to the new Animation
         **/
        virtual void setAnimation(SRef<Animation2d> newAnimation);

    protected:
        size_t currentKeyFrame;
        double lastFrameTime, thisFrameTime;
        double currentAnimationTime;
        SRef<Animation2d> animation;

    private:
};

#endif // ANIMATOR2D_H
