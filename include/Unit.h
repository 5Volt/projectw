#ifndef UNIT_H
#define UNIT_H

#include<GMath2d.h>
#include<GMathBezier.h>
#include<GObj2d.h>
#include<Path.h>
#include<PathFinder.h>
#include<DeleteEvent.h>
#include<ExplosionEvent.h>

class ContextInformation;
class GameInformation;

//TODO document
class Unit : public Object
{
    public:
        static constexpr GMath::vec2::valType TURNING_CIRCLE = 10, MAX_SPD = 0.5;
        static constexpr double FIRE_TIME = 1.0, RANGE = 30;
        static constexpr int STARTING_HEALTH = 80, SHOT_DAMAGE = 40;
        
        double lastFireTime;
        
        int faction;
        
        Unit();
        virtual ~Unit();

        virtual void draw(const ContextInformation& context);
        
        virtual void ai(const GameInformation&);
        
        virtual bool active();
        
        virtual void newTarget(SRef<Unit>, const GameInformation &);
        virtual void newTarget(GMath::vec2, const GameInformation &);
        
        //helper function
        SRef<Path> smoothPath(SRef<Path>);
        
        //TODO these functions may not belong here
        void applyDamage(int damage);
        
    protected:
        GMath::mat3 matrix;
        GMath::vec2 destination, velocity;
        
        int health;
        
        bool idle, alive;
        
        SRef<Path> path;
        
        SRef<Unit> myTarget;
        
        SRef<GObj2d> graphicsObject;
        
        SRef<Animation2d> walkAnimation, idleAnimation;
    private:
};

#endif // UNIT_H
