#ifndef SREF_H_INCLUDED
#define SREF_H_INCLUDED

#include<cstddef>
#include<iostream>
//TODO document
//TODO convert to generic base system
/**
 * The intention of this file is to provide automated garbage collection for arbitrary types.
 * This involves a 2 stage modification to the way in which normal types are referred to,
 * First, each object must track how many references there are to it,
 * Second, each reference must exist in stack space, and as part of it's own destructor, will check its referred contents to see if it is the last remaining reference
 **/

//forward declaration
template<class T>
class SRef;


/**
 * SObjWrapper is the layer which tracks references to an object, it must be passed a constructed object, and it will attatch to that object, 
 * will only every be accessed by methods of SRef, the reference class
 **/
class SObjWrap{
    
    template<typename T>
    friend class SRef;

    SObjWrap(void* obj):object(obj), numReferences(){}
    
    ~SObjWrap(){}
    
    template<typename T>
    T* getObjPointer(){
        return reinterpret_cast<T*>(object);
    }
        
private:
    //pointer will cause some double dereferncing, but it allows me to create references 
    //to objects which are already allocated in memory (e.g. sref<object> = new object;)
    void *object; 
    unsigned int numReferences;
};

/**
  SRef objects should be assignable in multiple ways
  1. from another SRef.
  2. from a pointer to a heap-allocated object
 **/

template<class T>
class SRef{
    template<typename U>
    friend class SRef;
    
protected:
    SObjWrap *obj;

    void decRef(){
        if(obj != NULL){
            obj->numReferences--;
            if(obj->numReferences == 0){
                delete obj->getObjPointer<T>();
                delete obj;
                obj = NULL;
            }
        }
    }
    
    void incRef(){
        if(obj != NULL){
            obj->numReferences++;
        }

    }
    
    
public:
    
    SRef():obj(NULL){}
    
    SRef(const SRef<T> &other):obj(NULL){
        *this = other;
    }
    
    SRef(T* other):obj(NULL){
        *this = other;
    }
    
    ~SRef(){
        decRef();
    }

    SRef<T> &operator=(const SRef<T> &other){
        decRef();
        obj = other.obj;
        incRef();
        return *this;
    }
    
    SRef<T> &operator=(T* other){
        decRef();
        obj = new SObjWrap(other);
        incRef();
        return *this;
    }
    
    template<typename U>
    bool operator==(const SRef<U> other)const{    
        return (obj == other.obj);
    }
    
    bool operator==(const T* other)const{
        if(obj == NULL){
            if(other == NULL)
                return true;
            else
                return false;
        }
        return (obj->getObjPointer<T>() == other);
    }
    
    bool operator!=(const SRef<T> other)const{
        return !(*this == other);
    }
    
    bool operator!=(const T* other)const{
        return !(*this == other);
    }
    
    T* operator->(){
        if(obj == NULL)
            return NULL;
        else
            return obj->getObjPointer<T>();
    }
    
    const T* operator->() const {
        if(obj == NULL)
            return NULL;
        else
            return obj->getObjPointer<T>();
    }
    
    T& operator*(){
        if(obj == NULL){
            return *static_cast<T*>(NULL);
         }else {
            return *(obj->getObjPointer<T>());
         }
    }
    
    
    const T& operator*() const{
        return *(*this);
    }
    
    SRef<T> clone(){
        return SRef<T>(new T(*(obj->getObjPointer<T>())));
    }
    
    template<typename U>
    operator SRef<U>(){
        SRef<U> casted;
        casted.obj = obj;
        casted.incRef();
        return casted;
    }
    
    public: unsigned int numRefs(){return obj->numReferences;}
    
};

#endif // SREF_H_INCLUDED
