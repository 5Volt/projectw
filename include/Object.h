#ifndef OBJECT_H
#define OBJECT_H

#include<GMath.h>
//TODO document
class Object
{
    public:
        Object():position(),rotation(){}
        virtual ~Object(){}
        
        GMath::vec2& getPosition(){
            return position;
        }
        const GMath::vec2& getPosition()const{
            return position;
        }
        GMath::mat3::valType& getRotation(){
            return rotation;
        }
        const GMath::mat3::valType& getRotation()const{
            return rotation;
        }
                
    protected:
        //position data
        GMath::vec2 position;
        GMath::mat3::valType rotation;
    private:
};

#endif // OBJECT_H
