#ifndef CONTEXTINFORMATION_H_INCLUDED
#define CONTEXTINFORMATION_H_INCLUDED

#include<GL/GL.h>
#include<NavMesh.h>
#include<ObjectSet.h>
#include<EventQueue.h>
/**
 * This struct contains information relating to a GView context (openGL related information)
 * including the ID of the associated shader, the uniform location to load MVP matrices for drawing
 * and the current VPMatrix and it's inverse.
 **/
struct ContextInformation{
    GLuint shaderID;
    GLuint MVPID;
    //TODO generalise for 2d/3d
    GMath::mat3 VPMatrix,IVPMatrix;
};
/**
 * This struct contains information relating to the logic of a Game object (logic related information)
 * including the the navigable areas of the world space and the objects active in the game
 **/ 
struct GameInformation{
    NavMesh navigation;
    ObjectSet units;
    EventQueue events;
};
#endif // CONTEXTINFORMATION_H_INCLUDED
