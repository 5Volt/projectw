#ifndef PATHFINDER_H
#define PATHFINDER_H

#include<vector>
#include<list>
#include<queue>
#include<SRef.h>
#include<NavMesh.h>
#include<GMath.h>
#include<Path.h>

//TODO document
struct PathFinder
{
        
        
    static SRef<Path> getOptimalPath(GMath::vec2, GMath::vec2, const NavMesh&);
    
    static SRef<NavMesh::sectorList> getCoarsePath(const NavMesh &, GMath::vec2, GMath::vec2);
    
    static SRef<Path> refinePath(SRef<NavMesh::sectorList>, const NavMesh &, GMath::vec2);
    
};

#endif // PATHFINDER_H
