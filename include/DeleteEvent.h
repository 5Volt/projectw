#ifndef DELETEEVENT_H
#define DELETEEVENT_H

#include <SRef.h>
#include <Object.h>
#include <ContextInformation.h>
#include <Event.h>
//NOTE name may be ambiguous
//TODO document
class DeleteEvent : public Event
{
    public:
        DeleteEvent(SRef<Object> toDelete, double time = 0);
        virtual ~DeleteEvent();
        
        virtual void execute(GameInformation &info);
        
    protected:
    private:
        SRef<Object> obj;
};

#endif // DELETEEVENT_H
