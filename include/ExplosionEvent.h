#ifndef EXPLOSIONEVENT_H
#define EXPLOSIONEVENT_H

#include<Unit.h>
#include<Event.h>
#include<ContextInformation.h>
#include<GMath.h>

class ExplosionEvent : public Event
{
    public:
        ExplosionEvent(double size, GMath::vec2 epicenter, int damage, double ntime = 0);
        virtual ~ExplosionEvent();
    protected:
        
        virtual void execute(GameInformation &info);
        
        double expSize, damage;
        GMath::vec2 position;
    private:
};

#endif // EXPLOSIONEVENT_H
