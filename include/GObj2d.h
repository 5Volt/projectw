#ifndef GOBJ2D_H
#define GOBJ2D_H

#include<SRef.h>
#include<GMesh2d.h>
#include<GMath.h>
#include<ContextInformation.h>
#include<Animation2d.h>
#include<Animator2d.h>
#include<ResourceManager.h>

#include<iostream>
#include<vector>
#include<map>
#include<string>
#include<GL/gl.h>

//TODO document
class GObj2d
{
    
    public:
        
        typedef SRef<GMesh2d> meshRef;
        GObj2d();
        GObj2d(const GObj2d &other);
        GObj2d(const std::string &fileName);
        virtual ~GObj2d();
        
        virtual void load(std::string);
        
        virtual void draw(const ContextInformation&);
        
        void setAnimation(SRef<Animation2d> newAnimation);
        
    protected:
    
    friend std::ostream &operator<<(std::ostream&, const GObj2d&);
    friend std::istream &operator>>(std::istream&, GObj2d&);
    
        std::vector<meshRef> meshes;
        std::vector<std::string> meshNames;
        size_t currentAnimation;
        Animator2d animator;
        GMath::mat3 modelMatrix;
        size_t numMeshes;
        
    private:
};

#endif // GOBJ2D_H
