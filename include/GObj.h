#ifndef GOBJ_H
#define GOBJ_H

#define GLEW_STATIC
#include<GL/glew.h>
//#include<GL/glu.h> // already included by glew

#include<fstream>
#include<vector>
#include"SRef.h"
#include"GMath.h"
#include"ContextInformation.h"

class GObj
{
public:
    
    GObj(){}
    
    /**
     * instantiates and loads mesh into memory
     **/
    GObj(std::string){}
    
    virtual ~GObj(){}
    
    /**
     * loads a model from file into graphics memory
     * this is the basic GOBj class, so it will load a basic model
     * ( no textures, no index buffer )
     **/
    virtual void load(std::string) = 0;
    
    /**
     * updates the representation in Graphics memory
     * should be called every time local data changes
     **/
    virtual void loadGMem(void) = 0;
    
    /**
     * saves the model from graphics memory into a file.
     * this is the basic GOBj class, so it will load a basic model
     * ( no textures, no index buffer )
     **/
    virtual void save(std::string) = 0;
    
    /**
     * draws this object in the currently active opengl context
     **/
    virtual void draw(const ContextInformation &) = 0;
    
    bool isLoaded();
    
protected:
    
    GLuint meshID,secondaryID, drawMode;
    bool loaded;
    GLsizei numVertices;
    
private:
    //nothing is private, this class is designed to be extendable 
    
};
#endif // GOBJ_H
