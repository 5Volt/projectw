#ifndef EVENTQUEUE_H
#define EVENTQUEUE_H

#include<queue>

#include<SRef.h>
#include<Event.h>

//Declare a function to allow sorting of referenced Events in priority queue
bool operator<(const SRef<Event> &lhs, const SRef<Event> &rhs);

class EventQueue
{
    public:
        EventQueue();
        virtual ~EventQueue();
        
        void addEvent(const SRef<Event> &newEvent)const;
        const SRef<Event> &nextEvent() const;
        SRef<Event> popNextEvent();
        
        bool isEmpty();
        
    protected:
        mutable std::priority_queue<SRef<Event> > events;
    private:
};

#endif // EVENTQUEUE_H
