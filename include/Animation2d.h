#ifndef ANIMATION2D_H
#define ANIMATION2D_H

#include<vector>
#include<string>
#include<iostream>
#include<fstream>
#include<ctime>

#include<GL/glfw.h>

#include<GMath.h>

//forward declare KeyFrame2d
struct KeyFrame2d;

/**
 * This class represents the bone structure(matrix offsets) of each element of a GObj2d.
 * It is time dependant, and may provide a different bone structure depending on the at which it is called
 * relative to the time at which it was instantiated
 * @author 5Volt
 **/
class Animation2d
{
    //TODO default animation offsets for static objects
    public:
        /**
         * Instantiates a default Animation2d (returns identity matrix for all queries)
         **/
        Animation2d();

        Animation2d(const Animation2d &other);
        /**
         * Instantiates an Animation2d by reading data from a file in the file system
         * @param   fileName    The path of the file to be read from
         **/
        Animation2d(const std::string fileName);

        virtual ~Animation2d();


        /**
         * Loads data from a file in the file system into an Animation2d
         * @param   filename    The path of the file to be loaded from
         **/
        virtual void load(const std::string filename);

        /**
         * Gets a keyFrame from the current animation
         * @param   frame   Index of the keyFrame to return
         **/
        virtual const KeyFrame2d &getFrame(const size_t frame);

        /**
         * Gets the number of KeyFrames in this animation
         **/
        virtual size_t numKeyFrames();

        virtual size_t getSize();

        virtual double length();

        /**
         * Inserts data into an ostream.
         * @param   out The ostream to be written to
         * @param   anim    The Animation2d who's information will be written
         **/
        friend std::ostream &operator<<(std::ostream& out, const Animation2d &anim);
        /**
         * Retrieves data from an istream.
         * @param   in  The istream to be written to
         * @param   anim    The Animation2d into which the information will be loaded
         **/
        friend std::istream &operator>>(std::istream& in, Animation2d &anim);
    protected:

        size_t numOffsets;
        std::vector<KeyFrame2d> keyFrames;
        double animationLength;
    private:
        //TODO maybe this isn't the best solution
        friend class Animator2d;
};

/**
 * This class represents the positions of every element in the GObj2d at a key point in time
 **/
struct KeyFrame2d{
    double time;
    std::vector<GMath::mat3> offsets;
    void setSize(const size_t newSize){
        std::cout<<"setting keyframe size: "<<newSize<<std::endl;
        offsets.resize(newSize);
    }
    friend std::ostream& operator<<(std::ostream & out, const KeyFrame2d &frame);
    friend std::istream& operator>>(std::istream & out, KeyFrame2d &frame);
};

#endif // ANIMATION2D_H
