#ifndef DAMAGEEVENT_H
#define DAMAGEEVENT_H

#include <SRef.h>
#include <Unit.h>
#include <Event.h>


class DamageEvent : public Event
{
    public:
        DamageEvent(SRef<Unit> newTarget, int amount, double time = 0);
        virtual ~DamageEvent();
    protected:
        
        SRef<Unit> target;
        int damage;
    private:
};

#endif // DAMAGEEVENT_H
