#ifndef GVIEW_H
#define GVIEW_H

#include"GMesh2d.h"
#include"GObj2d.h"
#include"GMath.h"
#include"SRef.h"
#include"ContextInformation.h"

#include<iostream>
#include<string>
#include<ctime>
#include<vector>

#define GLEW_STATIC
#include<GL/glew.h>
#include<GL/glfw.h>
#include<GL/gl.h> // already included by glfw
#include<GL/glu.h> // already included by glew
#include<cmath>
#include<fstream>

//TODO document
class GView{
    public:
        GView(const std::string,int = 3,int = 2);
        virtual ~GView();

        void setFrameRate(unsigned int);

        void loadShaderPackage(std::string);
        void compileShader(GLuint, std::istream&);

        void run();
        virtual void init() = 0;
        virtual void draw() = 0;

    protected:

        double period, time;
        bool running;

        //TODO generalise for 2d/3d
        //push down if possible.
        ContextInformation context;

    private:
};

#endif // GVIEW_H
