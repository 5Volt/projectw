#ifndef GMATHBEZIER_H_INCLUDED
#define GMATHBEZIER_H_INCLUDED

namespace GMath{
namespace Bezier{

//TODO document
//TODO n dimensional bezier
class Bezier{
    
public:
    Bezier(vec2 n1,vec2 n2,vec2 n3,vec2 n4): p0(n1), p1(n2),p2(n3),p3(n4)
    {}
    
    vec2 getPoint(vec2::valType interpolation){
        
        vec2 mp0,mp1,mp2,mp11,mp12;
        
        mp0 = p0 + (p1-p0)*interpolation;
        mp1 = p1 + (p2-p1)*interpolation;
        mp2 = p2 + (p3-p2)*interpolation;
        mp11 = mp0 + (mp1-mp0)*interpolation;
        mp12 = mp1 + (mp2-mp1)*interpolation;
        
        return mp11 + (mp12-mp11)*interpolation;
        
    }
    
private:
    vec2 p0,p1,p2,p3;
    
};

}
}


#endif // GMATHBEZIER_H_INCLUDED
