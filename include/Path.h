#ifndef PATH_H
#define PATH_H

#include<list>
#include<GMath.h>
//TODO document
/**
 * Path is a small Wrapper class for list<GMath::vec2> which can have many uses, including being the final product of a pathfinding algorithm
 **/

class Path{
public:
    Path();
    ~Path();
    //peek
    GMath::vec2 nextPoint() const;
    GMath::vec2 lastPoint() const;
    //puush and pop
    void pushLastPoint(const GMath::vec2&);
    GMath::vec2 popNextPoint();
    
    bool isEmpty() const;
    
    Path &operator=(const Path &);
    
private:

    std::list<GMath::vec2> points;
    
};

#endif // PATH_H
