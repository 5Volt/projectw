#ifndef EVENT_H
#define EVENT_H
#include<iostream>
class GameInformation;

class Event
{
    public:
        virtual ~Event(){};
        
        virtual void execute(GameInformation &info ) = 0;
        
        virtual double getTime() const {
            return time;
        }
        
    protected:
        double time;
        //even though this class is incomplete, I have a private constructor for good measure
        Event(double newTime = 0):time(newTime){}
    private:
};

#endif // EVENT_H
