#ifndef SAMPLEOBJECT_H_INCLUDED
#define SAMPLEOBJECT_H_INCLUDED

#include"SRef.h"

class SampleObject{
    public:
        SampleObject(){
            std::cout<<"sobj made"<<std::endl;
        }
        
        SampleObject(const SampleObject &other){
            std::cout<<"sobj copied"<<std::endl;
        }
        
        ~SampleObject(){
            std::cout<<"object destructed"<<std::endl;
        }
        
        void foo(){
            std::cout<<"foo called"<<std::endl;
        }
};

#endif // SAMPLEOBJECT_H_INCLUDED
