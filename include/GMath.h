#ifndef GMATH_H_INCLUDED
#define GMATH_H_INCLUDED

#include<iostream>
#include<cstddef>
#include<cstdarg>
#include<cmath>
//TODO document
namespace GMath{
//set the standard type of this library
typedef float gmath_default_type;

template<std::size_t width,typename type>
struct mat;
template<std::size_t dimensions, typename type>
struct vec;
//template declarations for operators
//vector additions
template<std::size_t L, typename T>
vec<L,T> operator+(const vec<L,T> &, const vec<L,T> &);
//subtraction
template<std::size_t L, typename T>
vec<L,T> operator-(const vec<L,T> &, const vec<L,T> &);
//multiplication(dot)
template<std::size_t L, typename T>
T operator*(const vec<L,T> &, const vec<L,T> &);
//multiplication(scalar)
template<std::size_t L, typename T>
vec<L,T> operator*(const vec<L,T> &, const T &);
//multiplication(matrix)
template<std::size_t L, typename T>
vec<L,T> operator*(const vec<L,T> &, const mat<L,T> &);
//comparison
template<std::size_t L, typename T>
bool operator==(const vec<L,T>&,const vec<L,T>&);
template<std::size_t L, typename T>
bool operator!=(const vec<L,T>&,const vec<L,T>&);
//insertion
template<std::size_t L, typename T>
std::ostream &operator<<(std::ostream&, const vec<L,T> &);
//retrieval
template<std::size_t L, typename T>
std::istream &operator>>(std::istream &, vec<L,T> &);

/**
 * WARNING!: adding/removing member variables will cause both vec and mat to be incompatible with openGL code. they are designed in such a way that
 * a pointer to a mat<W,T> is the same as a pointer to a contiguous array of T with size W*W.
 **/
template<std::size_t dimensions, typename type = gmath_default_type>
struct vec{    
    
    typedef type valType;

    //constructors
    //basic contructor
    vec(type , type, type, type);
    //arrayConstructor
    template<typename fromT>
    vec(fromT*);
    //copyctor
    template<size_t fromD, typename fromT>
    vec(const vec<fromD,fromT>&);
    //member functions
    void setAll(type);
    type length() const;
    //external operators    
    friend vec<dimensions,type> operator+ <dimensions,type>(const vec<dimensions,type> &, const vec<dimensions,type> &);
    friend vec<dimensions,type> operator- <dimensions,type>(const vec<dimensions,type> &, const vec<dimensions,type> &);
    friend type operator* <dimensions,type>(const vec<dimensions,type> &, const vec<dimensions,type> &);
    friend vec<dimensions,type> operator* <dimensions,type>(const vec<dimensions,type> &, const mat<dimensions,type> &);
    friend bool operator== <dimensions,type>(const vec<dimensions,type> &, const vec<dimensions,type> &);
    friend bool operator!= <dimensions,type>(const vec<dimensions,type> &, const vec<dimensions,type> &);
    friend std::ostream &operator<< <dimensions,type>(std::ostream &, const vec<dimensions,type> &);
    friend std::istream &operator>> <dimensions,type>(std::istream &, vec<dimensions,type> &);
    //member operators
    //wierdly, this cannot be resolved in some instances.
    template<size_t fromD, typename fromT>
    vec<dimensions,type> &operator=(const vec<fromD,fromT> &);
    vec<dimensions,type> &operator+=(const vec<dimensions,type> &);
    vec<dimensions,type> &operator-=(const vec<dimensions,type> &);
    vec<dimensions,type> &operator*=(const type &);
    vec<dimensions,type> &operator*=(const mat<dimensions,type> &);
    //type conversions
//    operator type*();
    
    type &operator[](size_t index){
        return vals[index];
    }

    const type &operator[](size_t index) const{
        return vals[index];
    }
    
    void clear();
    
    //specialised functions for aliasing x,y,z
    type &x(){
        return vals[0];
    }
    type &y(){
        return (dimensions>=2) ? vals[1]:vals[0];
    }
    type &z(){
        return (dimensions>=3) ? vals[2]:vals[0];
    }
    type &w(){
        return (dimensions>=4) ? vals[3]:vals[0];
    }    
    const type &x() const{
        return vals[0];
    }
    const type &y()const{
        return (dimensions>=2) ? vals[1]:vals[0];
    }
    const type &z()const{
        return (dimensions>=3) ? vals[2]:vals[0];
    }
    const type &w()const{
        return (dimensions>=4) ? vals[3]:vals[0];
    }
    
private:
    type vals[dimensions];
};

//manipulation functions

template<std::size_t length, typename type>
vec<length, type> normalise(vec<length,type>);

template<std::size_t W,typename T>
mat<W,T> operator+(const mat<W,T>&, const mat<W,T>&);
template<std::size_t W,typename T>
mat<W,T> operator-(const mat<W,T>&, const mat<W,T>&);
template<std::size_t W,typename T>
mat<W,T> operator*(const mat<W,T>&, const mat<W,T>&);
template<std::size_t W,typename T>
mat<W,T> operator*(const mat<W,T>&, const T&);
template<std::size_t W,typename T>
std::ostream &operator<<(std::ostream&, const mat<W,T>&);
template<std::size_t W,typename T>
std::istream &operator>>(std::istream&, const mat<W,T>&);



template<std::size_t width, typename type = gmath_default_type>
struct mat{
    
    typedef vec<width,type> rowType;    
    typedef type valType;
    
    mat();
    mat(type *);
    template<typename fromT>
    mat(const mat<width,fromT>&);
    
    template<typename fromT>
    mat<width,type> &operator =(const mat<width,fromT>&);
    
    mat<width,type> &operator +=(const mat<width, type>&);
    mat<width,type> &operator -=(const mat<width, type>&);
    mat<width,type> &operator *=(const mat<width, type>&);
    mat<width,type> &operator *=(const type&);
    
    mat<width,type> transpose() const;
    mat<width,type> &compose(const mat<width,type> &);
    
    mat<width,type> interpolate(const mat<width,type>& other,double factor);
    
    type* operator[](std::size_t);
    const type* operator[](std::size_t) const;
        
    void clear();
    
    type* toArray(){
        return &values[0][0];
    }
    
    const type* toArray() const {
        return &values[0][0];
    }
    
    private:
        
    type values[width][width];
    
};

//some common aliases
typedef vec<2> vec2;
typedef vec<3> vec3;
typedef vec<4> vec4;

typedef mat<2> mat2;
typedef mat<3> mat3;
typedef mat<4> mat4;

//some constants
const gmath_default_type PI = atan(1)*4;
}
//Manipulation functions

#include "GMath.tem"


#endif // GMATH_H_INCLUDED
