#ifndef GMesh_H
#define GMesh_H

#define GLEW_STATIC
#include<GL/glew.h>
#include<GL/glu.h> // already included by glew

#include<fstream>
#include<vector>
#include"SRef.h"
#include"GMath.h"
#include"ContextInformation.h"
/**
 * This abstract class represents the coordinate and colour information of a single graphical element.
 * It can be used to buffer information in graphics memory and generally perform any necessary interaction with openGL
 * which is required to draw complex information to the screen.
 * GMesh objects cannot be instantiated, to use a GMesh, it must be instantiated as a complete-type subclass
 **/
class GMesh
{
public:

    GMesh(): meshID(), secondaryID(),loaded(false), numVertices(){}

    /**
     * Instantiates and loads mesh into memory from a file in the file system
     * @param   filename    The path of the file from which this mesh will be loaded
     **/
    GMesh(std::string filename): meshID(), secondaryID(),loaded(false), numVertices(){}

    virtual ~GMesh(){}

    /**
     * loads a mesh from file into RAM and graphics memory
     * @param   filename    The path of the file from which this mesh will be loaded
     **/
    virtual void load(std::string filename) = 0;

    /**
     * updates the representation in Graphics memory
     * should be called every time local (RAM) data changes
     **/
    virtual void loadGMem() = 0;

    /**
     * draws this object in the currently active opengl context
     **/
    virtual void draw() = 0;

    /**
     * returns true if this GMesh is currently loaded into graphics memory
     **/
    bool isLoaded();

protected:

    GLuint meshID,secondaryID, drawMode;
    bool loaded;
    GLsizei numVertices;

private:
};
#endif // GMesh_H
