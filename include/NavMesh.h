#ifndef NAVMESH_H_INCLUDED
#define NAVMESH_H_INCLUDED

#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include<GMath.h>
#include<SRef.h>

//TODO document
#define VERT_PER_SECTOR 3

class Sector;
class NavMesh;

class NavMesh {

friend std::ostream &operator<<(std::ostream&, const NavMesh&);
friend std::istream &operator>>(std::istream&, NavMesh&);

private:
    
    struct Sector{
        static const size_t numVertices;
        size_t indices[VERT_PER_SECTOR], neighbours[VERT_PER_SECTOR];
    };
    
    
    bool inSector(size_t, const GMath::vec2&) const;
    
public:
    typedef std::vector<size_t> sectorList;
    
    
    //sector features
    size_t getSector(const GMath::vec2) const;
    SRef<sectorList> getNeighbours(const size_t) const;
    GMath::vec2 getCenter(const size_t) const;
    GMath::vec2 getClosest(const size_t, const GMath::vec2&) const;
    
    void load(const std::string);
    void save(const std::string) const;
    
    void printSector(const size_t, std::ostream&)const;

    
private:
    static Sector readSector(std::istream &);
    
    size_t numPoints;
    size_t numSectors;
    std::vector<GMath::vec2> points;
    std::vector<Sector> sectors;
    
};




#endif // NAVMESH_H_INCLUDED
