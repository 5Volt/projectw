#ifndef GVIEW2D_H
#define GVIEW2D_H

#include "GView.h"
#include <GMath2d.h>
#include "GMesh2d.h"
//TODO document
class GView2d : public GView
{
public:
    typedef SRef<GObj2d> objRef;
    typedef std::vector<objRef> objectList;
    
    GView2d(const std::string,int = 4,int = 2);
    virtual ~GView2d();
     
    virtual void init();
    virtual void draw();
    
    virtual void addObject(objRef newObj);
    
    void setCamera(const GMath::vec2 &pos, double zoom);
    
protected:
    
    objectList objects;

private:
};

#endif // GVIEW2D_H
