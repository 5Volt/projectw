#ifndef RESOURCEMANAGER_H_INCLUDED
#define RESOURCEMANAGER_H_INCLUDED

#include <string>
#include <map>
#include <SRef.h>

//forward declare to avoid circular inclusion
class GObj2d;
class GMesh2d;
class Animation2d;

//TODO document

namespace ResourceManager{
    extern const std::string meshDirectory, meshExtension;
    extern const std::string objectDirectory, objectExtension;
    extern const std::string animationDirectory, animationExtension;
    bool operator<(const std::string &lhs, const std::string &rhs);    
    //GMesh    
    SRef<GMesh2d> getMesh(const std::string& meshID);
    //GObj
    SRef<GObj2d> getObject(const std::string& objID);
    //Animation
    SRef<Animation2d> getAnimation(const std::string& animID);
} 

#endif // RESOURCEMANAGER_H_INCLUDED
