#ifndef GAME_H
#define GAME_H

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <SRef.h>
#include <GView2d.h>
#include <InputListener.h>
#include <Unit.h>
#include <ObjectSet.h>
#include <ContextInformation.h>

//TODO document
class Game : public GView2d, public InputListener
{
    public:
        typedef SRef<Unit> unitRef;
        Game(std::string name = "Game Window", int GLMaj = 3, int GLMin = 2);
        virtual ~Game();

        void loadMap(const std::string);

        virtual void init();
        virtual void draw();
        virtual unitRef choose(const int&, const int&);

        GMath::vec2 toWorldSpace(int, int);

        virtual void mouseClicked(int button);

    protected:

    GObj2d map;

    GLuint chooseShaderID;
    GLuint chooseColorID, chooseMVPID;

    unitRef selected;

    GameInformation info;

};

#endif // GAME_H
