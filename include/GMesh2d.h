#ifndef GMesh2d_H
#define GMesh2d_H

#include <GMesh.h>

/**
 * This class represents the coordinate and colour information of a single graphical element in a 2d openGL context.
 * It can be used to buffer information in graphics memory and generally perform any necessary interaction with openGL
 * which is required to draw complex information to the screen.
 **/
class GMesh2d : public GMesh
{
public:
    
    /**
     * Instantiates a default(empty) GMesh2d
     **/
    GMesh2d();
    /**
     * Loads information from a file in the file system into a new GMesh2d
     * @param   filename    The path of the file from which this GMesh2d will be loaded
     **/
    GMesh2d(const std::string &filename);
    /**
     * Instantiates a new GMesh2d to be identical to the provided GMesh2d
     * however the changes in the new mesh will not be reflected, either in local or graphics memory
     * on the original
     * @param   other   The GMesh2d which this GMesh2d will be a copy of
     **/
    GMesh2d(const GMesh2d &other);

    virtual ~GMesh2d();
        
    virtual void load(std::string filename);
    virtual void loadGMem();

    virtual void draw();
    
    
    bool isLoaded();
    
protected:
    /**
     * Inserts data into an ostream.
     * @param   out The ostream to be written to
     * @param   mesh    The GMesh2d who's information will be written
     **/
    friend std::ostream &operator<<(std::ostream& out, const GMesh2d& mesh);
    /**
     * Retrieves data from an istream.
     * @param   in  The istream to be written to
     * @param   mesh    The GMesh2d into which the information will be loaded
     **/
    friend std::istream &operator>>(std::istream& in, GMesh2d& mesh);
    //NOTE these mean that there will always be one copy of vertex/color data on both RAM and GRAM
    std::vector<GMath::vec2::valType> vertexArray;
    std::vector<GMath::vec3::valType> colorArray;

private:
};



#endif // GMesh2d_H
