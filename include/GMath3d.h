#ifndef GMATH3D_H_INCLUDED
#define GMATH3D_H_INCLUDED
//TODO document
/**
 * This file declares and defines some common and useful functions to operator on 
 * 3d standard or homogenous coordinates i.e. predominantly vec3 and mat4.
 * Ideally this file will be included instead of the GMath-Core library when required. 
**/

/**NB regarding extensions:
 * the extended libraries will not use specific namespacing either using the c++ namespace
 * or manually(e.g. Rotate3d(mat4) instead of Rotate(mat4)).
**/

//include the core library
#include"GMath.h"
namespace GMath{
//vector properties
template<typename T>
T azimuth(const vec<3,T> &v){
    return atan2(v.x(),v.z());
}

template<typename T>
T elevation(const vec<3,T> &v){
    return sin(v.y()/v.length());
}
}
#endif // GMATH3D_H_INCLUDED
