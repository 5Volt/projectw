#include "GObj2d.h"

using namespace std;
using namespace GMath;

GObj2d::GObj2d(): meshes(),currentAnimation(),animator(),modelMatrix(),numMeshes(){

}

GObj2d::GObj2d(const GObj2d &other): GObj2d(){
    *this = other;
}

GObj2d::GObj2d(const std::string &fileName): GObj2d(){
    load(fileName);
}

GObj2d::~GObj2d(){}

void GObj2d::load(string filename){
    fstream file(filename.c_str());
    
    file>>(*this);
    
    file.close();
}

void GObj2d::draw(const ContextInformation &context){
        
    mat3 MVPMatrix = context.VPMatrix * modelMatrix;
    animator.update();
    for(size_t x = 0; x < numMeshes; x++){
        mat3 LMVPMatrix = MVPMatrix * animator.getOffset(x);
        glUniformMatrix3fv(context.MVPID,9, GL_FALSE,LMVPMatrix.toArray());
        meshes[x]->draw();
    }
    
}

void GObj2d::setAnimation(SRef<Animation2d> newAnimation){
    animator.setAnimation(newAnimation);
}

//stream operators

ostream &operator<<(ostream &out, const GObj2d &obj){
    out<<"#OBJECT2D"<<endl;
    out<<obj.numMeshes<<endl;
    
    out<<"#MESHES"<<endl;
    for(size_t x = 0; x < obj.numMeshes; x++){
        out<<obj.meshNames[x]<<endl;
    }
    
    return out;
}

istream &operator>>(istream &in, GObj2d &obj){
    static string headerCheck;
    
    in>>headerCheck;
    
    if(headerCheck.compare("#OBJECT2D") != 0)
        return in;

    in>>obj.numMeshes;
    in>>headerCheck;
    if(headerCheck.compare("#MESHES") != 0)
        return in;
    obj.meshes.clear();
    obj.meshes.resize(obj.numMeshes);
    obj.meshNames.resize(obj.numMeshes);
    for(size_t x = 0; x < obj.numMeshes; x++){
        string nextMesh;
        in>>nextMesh;
        obj.meshNames[x] = nextMesh.c_str();
        obj.meshes[x] = ResourceManager::getMesh(nextMesh);
    }
    
    return in;
}
