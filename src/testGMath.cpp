#include<cstddef>
#include<assert.h>
#include<iostream>
#include"GMath.h"

using namespace std;
using namespace GMath;

int main(){
    //test begin
{
    //test construction
    cout<<"testing construction"<<endl;
    //basic construction
    vec<3,int> vec1;
    //array construction
    float newVals[] = {1.0f,2.0f,3.0f};
    vec<3,float> vec2(newVals);
    //copy construction
    //implicit conversion, float->int
    vec<3,int> vec3(vec2);

    assert(vec1[0] == 0);
    assert(vec1[1] == 0);
    assert(vec1[2] == 0);

    assert(vec2[0] == 1);
    assert(vec2[1] == 2);
    assert(vec2[2] == 3);

    assert(vec3[0] == 1);
    assert(vec3[1] == 2);
    assert(vec3[2] == 3);

    cout<<"*Test Passed*"<<endl<<endl;
}
{
    //test assignment operator
    cout<<"testing assignment operator"<<endl;
    int vals[] = {1,2,3};
    vec<3,float> vec1,vec2(vals);
    vec<3,int> vec3,vec4(vals);
    //regular assignment
    vec1 = vec2;
    //conversion and assignment
    //implicit conversion int->float
    vec3 = vec1;
    assert(vec1[0] == 1);
    assert(vec1[1] == 2);
    assert(vec1[2] == 3);

    assert(vec3[0] == 1);
    assert(vec3[1] == 2);
    assert(vec3[2] == 3);

    assert(vec2[0] == 1);
    assert(vec2[1] == 2);
    assert(vec2[2] == 3);
    cout<<"*Test Passed*"<<endl<<endl;
}
{
    //test comparison operators
    cout<<"testing comparison operators"<<endl;
    float vals[] = {1,2,4,5,1};
    vec<5,float> vec1,vec2,vec3(vals);

    assert(vec1 == vec2);
    assert(!(vec1 == vec3));
    assert(!(vec3 == vec2));
    assert(!(vec1 != vec2));
    assert(vec1 != vec3);
    assert(vec3 != vec2);
    vec2 = vec3;
    assert(vec2 == vec3);
    assert(vec3 == vec2);
    assert(vec2 != vec1);
    cout<<"*Test Passed*"<<endl<<endl;
}
{
    //test addition subtraction
    cout<<"testing addition/subtraction"<<endl;
    float vals[] = {1,1,1};
    vec<3,int> veca(vals),vecb(vals),vecc(vals);
    
    //regular addition
    veca = vecb+vecc;
    assert(veca[0] == 2);
    assert(veca[1] == 2);
    assert(veca[2] == 2);
    //compound addition
    veca += vecb;
    assert(veca[0] == 3);
    assert(veca[1] == 3);
    assert(veca[2] == 3);
    //regular addition(self)
    veca = veca + veca;
    assert(veca[0] == 6);
    assert(veca[1] == 6);
    assert(veca[2] == 6);
    //compound addition(self)
    vecb += vecb;
    assert(vecb[0] == 2);
    assert(vecb[1] == 2);
    assert(vecb[2] == 2);
    //regular subtraction
    vecc = veca - vecb;
    assert(vecc[0] == 4);
    assert(vecc[1] == 4);
    assert(vecc[2] == 4);
    //compound subtraction
    veca -= vecb;
    assert(veca[0] == 4);
    assert(veca[1] == 4);
    assert(veca[2] == 4);
    //regular subtraction(self)
    vecb = veca-vecb;
    assert(vecb[0] == 2);
    assert(vecb[1] == 2);
    assert(vecb[2] == 2);
    //regular subtraction(self)
    vecc -= vecc;
    assert(vecc[0] == 0);
    assert(vecc[1] == 0);
    assert(vecc[2] == 0);
    cout<<"*Test Passed*"<<endl<<endl;
}
{
    //test multiplication
    cout<<"testing mulitplication"<<endl;
    int valsa[] = {1,2,3};
    vec<3,int> veca(valsa);
    int valsb[] = {3,2,1};
    vec<3,int> vecb(valsb),vecc;
    
    
    //vector multiplication
    assert(vecb*veca == 10);
    assert(vecb*vecb == 14);
    //scalar multiplication
    vecc = veca * 2;
    assert(vecc[0] == 2);
    assert(vecc[1] == 4);
    assert(vecc[2] == 6);
    vecc = veca;
    vecc = 2 * veca;
    assert(vecc[0] == 2);
    assert(vecc[1] == 4);
    assert(vecc[2] == 6);
    //compound scalar multiplication
    vecc *= 3;
    assert(vecc[0] == 6);
    assert(vecc[1] == 12);
    assert(vecc[2] == 18);
    
    
    cout<<"*Test Passed*"<<endl<<endl;
    
}
{
    //test index operator
    cout<<"testing index operator"<<endl;
    vec3 v1;
    v1[0] = 1;
    v1[1] = 2;
    v1[2] = 3;
    assert(v1[0]==1);
    assert(v1[1]==2);
    assert(v1[2]==3);
    
    assert(v1.x()==1);
    assert(v1.y()==2);
    assert(v1.z()==3);
    assert(v1.w()==1);
    

    
    const vec3 v2;
    
    assert(v2[0] == 0);
    assert(v2[1] == 0);
    assert(v2[2] == 0);
    cout<<"*Test Passed*"<<endl<<endl;
}
    
{
    //test matrix multiplication
    cout<<"testing matrix multiplication"<<endl;
    float valsa[] = {1,1,4,1,2,9,3,5,4,8,1,2,3,1,9,8},
           valsb[] = {2,3,1,2,5,2,5,1,1,1,9,3,8,4,8,4};
    
    mat4 mata(valsa), matb(valsb);
    
    mat4 matc(matb);
    
    mata *= matb;
    
    cout<<mata<<endl;
    cout<<"*Test Passed*"<<endl<<endl;
}

{
    //test matrix interpolation
    cout<<"testing matrix interpolation"<<endl;
    mat<3,int> mata, matb;
    matb = mata = mat<3,int>();
    matb[0][0] = matb[1][1] = 5;
    mat<3,int> mati = mata.interpolate(matb, 0.5);
    
    assert(mati[0][0] == 3);
    assert(mati[1][1] == 3);
    assert(mati[0][1] == 0);
    
    cout<<"*Test Passed*"<<endl;
}
    
    cout<<"***All Tests Passed***"<<endl;

    return 0;
}
