#include<iostream>
#include<Game.h>
#include<Unit.h>

using namespace std;

int main(void){

    Game mainGame;

    mainGame.loadShaderPackage("shaders/color2dShaderPack");

    mainGame.loadMap("maps/sampleMap.map");

    float pos[] = {1,1};
    mainGame.setCamera(pos,0.01);

    mainGame.setFrameRate(30);

    mainGame.run();

}
