#include "Game.h"

using namespace std;
using namespace GMath;
typedef Game::unitRef unitRef;

Game::Game(std::string name, int GLMaj, int GLMin): GView2d(name, GLMaj, GLMin), InputListener(), selected(){
    //aquire the choose shader on construction. this shader is tightly coupled with the game class.
    //NOTE to that end, perhaps the glsl for the choose shader should even be kept within the class specification
    loadShaderPackage("shaders/choose2dShaderPack");
    chooseColorID = glGetUniformLocation(context.shaderID, "choose_color");
    chooseMVPID = glGetUniformLocation(context.shaderID, "MVP");
    chooseShaderID = context.shaderID;
}

Game::~Game(){
    //dtor
}

void Game::loadMap(const string fileName){

    string headerCheck;

    ifstream file(fileName.c_str());

    static const string expectedHeader("#MAP");

    file>>headerCheck;

    if(headerCheck.compare(expectedHeader) != 0)
        return;

    file>>info.navigation;

    file>>map;


    file.close();

}

void Game::init(){
    SetFocus();
    unitRef unit = new Unit();
    info.units.insert(unit);
    //todo remove public members
    unit->faction = 0;
    unit = new Unit();
    info.units.insert(unit);
    unit->faction = 1;
    info.units.setHashSize(10);
}

void Game::draw(){

    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(context.shaderID);

    info.units.updateHash();

    for(auto it = info.units.begin(); it != info.units.end(); it++){
        unitRef curr= *it;
        curr->ai(info);
    }
    //perform events
    //NOTE: uses short circuit evaluation to avoid null dereference.
    bool noMoreEvents(false);
    while(!(info.events.isEmpty() || noMoreEvents)) {
        SRef<Event> ev = (info.events.nextEvent());
        if(ev->getTime() < time){
            info.events.popNextEvent();
            ev->execute(info);
        } else {
            noMoreEvents = true;
        }
    }
    //begin drawing
    //draw background
    map.draw(context);

    //draw units
    for(auto it = info.units.begin(); it != info.units.end(); it++){
        unitRef curr = *it;
        curr->draw(context);
    }
}

vec2 Game::toWorldSpace(int xPos, int yPos){

    //FIXME only works for camera at (0,0)
    int width,height;
    glfwGetWindowSize(&width,&height);
    vec2 newPoint;
    if(mouseX > width/2)
        newPoint.x() = xPos-width/2;
    else
        newPoint.x() = -(width/2 - xPos);

    if(mouseY > height/2)
        newPoint.y() = -(yPos-height/2);
    else
        newPoint.y() = height/2 - yPos;

    newPoint.x() *= (static_cast<vec2::valType>(1)/(width/2));
    newPoint.y() *= (static_cast<vec2::valType>(1)/(height/2));

    newPoint = vec3(newPoint.x(), newPoint.y(), 1) * context.IVPMatrix;

    return newPoint;

}

void Game::mouseClicked(int button){
    cout<<"mouse clicked"<<endl;
    if(button == GLFW_MOUSE_BUTTON_LEFT){
        selected = choose(mouseX, mouseY);
    } else {
        if(selected != NULL){
        unitRef target = choose(mouseX, mouseY);
            if(target != NULL)
                selected->newTarget(target, info);
            else
                selected->newTarget(toWorldSpace(mouseX, mouseY), info);
        }
    }
}

unitRef Game::choose(const int &xPos, const int &yPos){

    //FIXME only supports up to 256 objects at the moment
    //FIXME fix to use int vector

    glUseProgram(chooseShaderID);
    int unit = 0;
    glClear(GL_COLOR_BUFFER_BIT);
    vec3 col;
    ObjectSet::iterator it = info.units.begin();

    //save original mvpid and set mvp id to choose shader variant
    GLuint originalMVPID = context.MVPID;
    context.MVPID = chooseMVPID;

    while(it != info.units.end()){
        unit++;
        col[0] = static_cast<float>(unit)/256.0f;
        glUniform3fv(chooseColorID,3,&col[0]);
        unitRef curr = *it;
        curr->draw(context);
        ++it;
    }

    //restore original MVPID
    context.MVPID = originalMVPID;

    //retreive draw information from the frame buffer
    unsigned char pixel[3];

    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    glReadPixels(xPos, viewport[3] - yPos, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);

    int selected = pixel[0] - 1;
    cout<<"selected unit "<<selected<<endl;

    return (selected >= 0)? static_cast<unitRef>(info.units[selected]): unitRef();

}

