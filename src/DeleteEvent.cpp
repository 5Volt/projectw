#include "DeleteEvent.h"
//TODO remove
#include<iostream>

using namespace std;

DeleteEvent::DeleteEvent(SRef<Object> toDelete, double newTime):obj(toDelete)
{   
    time = newTime;
}

DeleteEvent::~DeleteEvent()
{
    //dtor
}

void DeleteEvent::execute(GameInformation &info){
    info.units.remove(obj);
}
