
#include"NavMesh.h"

using namespace std;
using namespace GMath;

//define static
const size_t NavMesh::Sector::numVertices = VERT_PER_SECTOR;

///helper methods

bool NavMesh::inSector(size_t sector, const vec2 &pos) const {
    const vec2 &pA = points[sectors[sector].indices[0]];
    const vec2 &pB = points[sectors[sector].indices[1]];
    const vec2 &pC = points[sectors[sector].indices[2]];
    
    bool b0 = (vec2(pos.x() - pA.x(), pos.y() - pA.y()) * vec2(pA.y() - pB.y(), pB.x() - pA.x()) > 0);
    bool b1 = (vec2(pos.x() - pB.x(), pos.y() - pB.y()) * vec2(pB.y() - pC.y(), pC.x() - pB.x()) > 0);
    bool b2 = (vec2(pos.x() - pC.x(), pos.y() - pC.y()) * vec2(pC.y() - pA.y(), pA.x() - pC.x()) > 0);
    return (b0 == b1 && b1 == b2);
    
}

///public methods
size_t NavMesh::getSector(const vec2 point) const{
    size_t location(numSectors);
    bool found(false);
    for(size_t x = 0; !found && x < numSectors; x++){
        if(inSector(x, point)){
            found = true;
            location = x;
        }
    }
    return location;
}

SRef<NavMesh::sectorList> NavMesh::getNeighbours(const size_t sector) const{
    SRef<sectorList> returnList = new sectorList();
    
    returnList->reserve(3);
    for(int x = 0; x < 3; x++)
        returnList->push_back(sectors[sector].neighbours[x]);
        
    return returnList;
}

vec2 NavMesh::getCenter(const size_t sect) const{
    const Sector &local = sectors[sect];
    vec2 center;
    
    for(size_t x = 0; x < Sector::numVertices; x++){
        center += points[local.indices[x]];
    }
    
    center *= 1.0/Sector::numVertices;
    
    return center;
}

vec2 NavMesh::getClosest(const size_t sect, const vec2& toPoint) const{
    const size_t *indices(& sectors[sect].indices[0]);
    size_t closestIndex(indices[0]);
    
    double distance((toPoint-points[indices[0]]).length());
    for(size_t x = 0; x < Sector::numVertices; x++){
        if(distance > (toPoint-points[indices[x]]).length()){
            distance = (toPoint-points[indices[x]]).length();
            closestIndex = indices[x];
        }
    }
    
    return points[indices[closestIndex]];
}

istream &operator>>(istream& in, NavMesh& nmesh){
    
    static string headerCheck;
    static const string expectedHeader("#NAVMESH");
    in>>headerCheck;    
    if(headerCheck.compare(expectedHeader) != 0)
        return in;
    
    in>>nmesh.numPoints>>nmesh.numSectors;
    
    static const string expectedSection("#VERTICES");
    in>>headerCheck;
    if(headerCheck.compare(expectedSection) != 0)
        return in;
    
    nmesh.points.clear();
    static vec2 nextVec;
    for(size_t x = 0; x < nmesh.numPoints; x++){
        in>>nextVec;
        nmesh.points.push_back(nextVec);
    }
    
    static const string expectedSection2("#SECTORS");
    in>>headerCheck;
    if(headerCheck.compare(expectedSection2) != 0)
        return in;
    
    nmesh.sectors.clear();
    for(size_t x = 0; x < nmesh.numSectors; x++){
        nmesh.sectors.push_back(NavMesh::readSector(in));
    }
    
    return in;
}

void NavMesh::load(const string filename){
    ifstream file(filename.c_str());
    
    file>>*this;
    
    file.close();
    
}
    
    
ostream &operator<<(ostream& out, const NavMesh& nmesh){
    
    out<<"#NAVMESH"<<endl;
    
    out<<nmesh.numPoints<<endl;
    out<<nmesh.numSectors<<endl;
    
    out<<"#VERTICES"<<endl;
    
    for(size_t x = 0; x < nmesh.numPoints; x++)
        out<<nmesh.points[x]<<' ';
    out<<endl;
    
    out<<"#SECTORS"<<endl;
    
    for(size_t x = 0; x < nmesh.numSectors; x++)
        nmesh.printSector(x, out);
    out<<endl;
    
    return out;
    
}

void NavMesh::printSector(size_t index, ostream &out) const{
    out<<'[';
    for(size_t x = 0; x < Sector::numVertices-1; x++)
        out<<sectors[index].indices[x]<<' ';
    out<<sectors[index].indices[Sector::numVertices-1]<<']'<<' ';
    out<<'{';
    for(size_t x = 0; x < Sector::numVertices-1; x++)
        out<<sectors[index].neighbours[x]<<' ';
    out<<sectors[index].neighbours[Sector::numVertices-1]<<'}'<<' ';
    
}

NavMesh::Sector NavMesh::readSector(istream &in){
    
    static const std::string delim("[]{}, \n");
    ///READ INDICES
    Sector returnSector;
    //read sector point indices
    for(std::size_t x = 0; x< Sector::numVertices; x++){
        //run out the stream until we get to a non-delimiter
        while(delim.find(static_cast<char>(in.peek())) != std::string::npos) in.get();
        in>>returnSector.indices[x];
    }
    //run out the rest of the stream until a closing bracket
    while( in.peek() == ' ') in.get();
    
    if(in.peek() == ']') in.get();
    ///READ LINKS
    //read sector neighbour indices
    for(std::size_t x = 0; x< Sector::numVertices; x++){
        //run out the stream until we get to a non-delimiter
        while(delim.find(static_cast<char>(in.peek())) != std::string::npos) in.get();
        in>>returnSector.neighbours[x];
    }
    //run out the rest of the stream until a closing bracket
    while( in.peek() == ' ') in.get();
    if(in.peek() == '}') in.get();
    
    
    return returnSector;
}

void NavMesh::save(const string filename) const{
    
    ofstream file(filename.c_str());
    
    file<<*this;
    
    file.close();
    
}


