#include "Path.h"

using namespace std;
using namespace GMath;

Path::Path(){}

Path::~Path(){}


vec2 Path::nextPoint() const{
    return points.front();
}

vec2 Path::lastPoint() const{
    return points.back();
}


void Path::pushLastPoint(const vec2& newPoint){
    points.push_back(newPoint);
}

vec2 Path::popNextPoint(){
    vec2 ret = points.front();
    points.pop_front();
    
    return ret;
}

bool Path::isEmpty() const{
    return points.empty();
}

Path &Path::operator=(const Path &other){
    points = other.points;
    return *this;
}
