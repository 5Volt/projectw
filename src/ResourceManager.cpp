#include <ResourceManager.h>
#include <GMesh2d.h>
#include <GObj2d.h>
#include <Animation2d.h>

namespace ResourceManager{
using namespace std;

    map<string, SRef<GMesh2d> > loadedMeshes;
    map<string, SRef<GObj2d> > loadedObjects;
    map<string, SRef<Animation2d> > loadedAnimations;
    const string meshDirectory("./meshes2d/"), meshExtension(".m");
    const string objectDirectory("./models2d/"), objectExtension(".o");
    const string animationDirectory("./animations2d/"), animationExtension(".a");


bool operator<(const string &lhs, const string &rhs){
    return lhs.compare(rhs) < 0;
}
//GMesh
SRef<GMesh2d> getMesh(const string &meshID){
    if(loadedMeshes[meshID] == NULL){
        //mesh not yet loaded
        string fullName = meshDirectory;
        fullName.append(meshID);
        fullName.append(meshExtension);
        loadedMeshes[meshID] = new GMesh2d(fullName);
    }
    return loadedMeshes[meshID];
}
//GObj
SRef<GObj2d> getObject(const string& objID){
    if(loadedObjects[objID] == NULL){
        string fullName = objectDirectory;
        fullName.append(objID);
        fullName.append(objectExtension);
        loadedObjects[objID] = new GObj2d(fullName);
    }
    return loadedObjects[objID].clone();
}
//Animation
SRef<Animation2d> getAnimation(const string &animID){
    if(loadedAnimations[animID] == NULL){
        //mesh not yet loaded
        string fullName = animationDirectory;
        fullName.append(animID);
        fullName.append(animationExtension);
        loadedAnimations[animID] = new Animation2d(fullName);
    }
    return loadedAnimations[animID];
}
}
