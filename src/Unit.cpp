#include "Unit.h"

using namespace std;
using namespace GMath;

//Define Unit statics

Unit::Unit(): Object(), matrix(),destination(),health(STARTING_HEALTH)
{   
    graphicsObject = new GObj2d();
    path = new Path();
//    graphicsObject = new GObj2d("models2d/model.o");
    graphicsObject = ResourceManager::getObject("model");
   
    lastFireTime = 0;
    
    
    walkAnimation = ResourceManager::getAnimation("oneOffsetSpin");
    idleAnimation = SRef<Animation2d>();
}

Unit::~Unit()
{
    //dtor
}

void Unit::draw(const ContextInformation &context){
    //NOTE inefficient copy context. 
    //perhaps mutable matrix stack?
    ContextInformation newContext = context;
    
    newContext.VPMatrix *= matrix;
    graphicsObject->draw(newContext);
}

void Unit::ai(const GameInformation &gameinfo){
    
    if(health <= 0){
        gameinfo.events.addEvent(new DeleteEvent(gameinfo.units.getObjectAt(this)));
        alive = false;
        return;
    }
    
    list<SRef<Object> > objects = gameinfo.units.getAdjacent(position,RANGE);
    for(auto it = objects.begin(); it != objects.end(); ++it){
        if(*(it) == myTarget){
            if(glfwGetTime() - lastFireTime > FIRE_TIME){
                cout<<"BOOM!"<<endl;
                gameinfo.events.addEvent(new ExplosionEvent(10,myTarget->getPosition(),SHOT_DAMAGE,glfwGetTime() + 0.5));
                lastFireTime = glfwGetTime();
            }
        }
    }
    
    while((position -destination).length()<MAX_SPD && !idle){
        if(!path->isEmpty()){
            destination = path->popNextPoint();
        }else{ 
            idle = true;
            graphicsObject->setAnimation(NULL);
        }
    }
    
    matrix = mat3();
    
    vec2 toDestination = destination - position;
    
    rotation = azimuth(toDestination);
    
    rotate(matrix, rotation);
    
    //NOTE Bizzarely. "vec2 myDirection = up*matrix" does not work(undefined reference error)
    vec2 myDirection(vec3(0,1,1)*matrix);
        
    if((position -destination).length()>MAX_SPD){
        velocity = myDirection * MAX_SPD; 
    } else { 
        velocity = vec2();
        position = destination;
        idle = path->isEmpty();
    }
        
    position += velocity;
    
    translate(matrix, position);
}

bool Unit::active(){
    return alive;
}

void Unit::newTarget(vec2 target, const GameInformation &gameinfo){
    path = smoothPath(PathFinder::getOptimalPath(position, target, gameinfo.navigation));
    graphicsObject->setAnimation(walkAnimation);
    idle = false;
}


void Unit::newTarget(SRef<Unit> target, const GameInformation &gameinfo){
    //TODO target units
    cout<<"target GET!"<<*target->graphicsObject<<endl;
    myTarget = target;
}

SRef<Path> Unit::smoothPath(SRef<Path> p){
    SRef<Path> smoothed = new Path();
    SRef<Path> original = p.clone();
    vec2 p0, p1, p2, p3;
    vec2 dirAtEnd,dirAtStart,toDestination;
    //initialise p3 as the starting point
    p3 = position;
    //initialise the current direction
    mat3 rotMatrix;
    rotate(rotMatrix, rotation);
    dirAtEnd = normalise(vec2(vec3(0,1,0)*rotMatrix));
    
    while(!original->isEmpty()){

        
        //get facing directions at either end of the segment
        dirAtStart = dirAtEnd;
        toDestination = original->nextPoint() - p3;
        dirAtEnd = normalise(toDestination);

        //get the control points for this segment
        p0 = p3;
        p1 = p0 + dirAtStart*TURNING_CIRCLE;
        p3 = original->popNextPoint();
        p2 = p3 - dirAtEnd*TURNING_CIRCLE;
    
        Bezier::Bezier curve(p0,p1,p2,p3);
        for(mat2::valType x = 0; x < 1.0; x+=0.01){
            smoothed->pushLastPoint(curve.getPoint(x));
        }
    }
    
    return smoothed;
}

void Unit::applyDamage(int damage){
    health -= damage;
}
