#include<iostream>

#include<SRef.h>

using namespace std;

class obj {
    double field;
};
class obj2 : public obj{
    int field2;
};

int main(){
    cout<<"Testing SRef"<<endl;
    
    {
        cout<<"Testing assignment from new operator"<<endl;
        SRef<obj2> ref1 = new obj2();
        cout<<"testing cast"<<endl;
        SRef<obj> ref2 = ref1;
        cout<<"testing assignment of original type"<<endl;
        SRef<obj2> ref3 = ref1;
        cout<<"testing assignment of post cast type"<<endl;
        SRef<obj> ref4 = ref2;
        
    }
}
