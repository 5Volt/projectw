#include<InputListener.h>

//define static
InputListener *InputListener::focus = NULL;
bool InputListener::mouseDown;
int InputListener::buttonDown;


InputListener::InputListener(){
    SetFocus();
}

void InputListener::keyChanged(int key, int action){
    //TODO fix new callback prototype
    if(action == GLFW_PRESS)
        focus->keyPressed(key);
    else if(action == GLFW_RELEASE)
        focus->keyReleased(key);
}

void InputListener::mouseButtonChanged(int button, int action){
    //TODO fix new callback prototype
    if(action == GLFW_PRESS){
        focus->mousePressed(button);
        mouseDown = true;
        buttonDown = button;
    } else if(action == GLFW_RELEASE){
        focus->mouseReleased(button);
        if(mouseDown && button == buttonDown){
            focus->mouseClicked(button);
        }
        mouseDown = false;
    }
}

void InputListener::mousePosChanged(int X, int Y){
    //TODO fix new callback prototype
    focus->mouseX = X; focus->mouseY = Y;
    focus->mouseMoved(X, Y);
    if(mouseDown){
        focus->mouseDragged(X, Y);
    }
}

void InputListener::SetFocus(){
    focus = this;
    //TODO fix this focus shit
    glfwSetKeyCallback(keyChanged);
    glfwSetMouseButtonCallback(mouseButtonChanged);
    glfwSetMousePosCallback(mousePosChanged);
}
