#include "PathFinder.h"

using namespace std;
using namespace GMath;

SRef<Path> PathFinder::getOptimalPath(vec2 from, vec2 to, const NavMesh &mesh){
    SRef<NavMesh::sectorList> coarse = getCoarsePath(mesh, from, to);
        
    cout<<endl;
    return refinePath(coarse, mesh, to);
    
}

SRef<NavMesh::sectorList> PathFinder::getCoarsePath(const NavMesh &mesh, vec2 from, vec2 to){
    //TODO loop detection
    typedef SRef<NavMesh::sectorList> listRef;
    
    queue<NavMesh::sectorList, list<NavMesh::sectorList> > paths;
    
    NavMesh::sectorList nextPath;
    nextPath.push_back(mesh.getSector(from));
    
    paths.push(nextPath);
    
    while(mesh.getSector(to) != nextPath.back()){
        NavMesh::sectorList children = *(mesh.getNeighbours(nextPath.back()));
        for(NavMesh::sectorList::iterator it = children.begin(); it != children.end(); it++){
            NavMesh::sectorList newPath = nextPath;
            newPath.push_back(*it);
            paths.push(newPath);
        }
        
        nextPath = paths.front();
        paths.pop();
        
    }
       
    listRef final = new NavMesh::sectorList(nextPath);
    
    return final;
}

SRef<Path> PathFinder::refinePath(SRef<NavMesh::sectorList> coarsePath, const NavMesh &mesh,vec2 destination){
    
    SRef<Path> newPath = new Path();
    for(size_t x = 1; x < coarsePath->size()-1; x++)
        newPath->pushLastPoint(mesh.getClosest((*coarsePath)[x],destination));
    
    newPath->pushLastPoint(destination);
    
    return newPath;
}
