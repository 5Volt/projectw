#include "DamageEvent.h"

DamageEvent::DamageEvent(SRef<Unit> newTarget, int amount, double time) : Event(time), target(newTarget), damage(amount)
{
    time = time;
}

DamageEvent::~DamageEvent()
{
    //dtor
}
