#include "Animation2d.h"

using namespace std;
using namespace GMath;

Animation2d::Animation2d():numOffsets(),keyFrames(),animationLength()
{}

Animation2d::Animation2d(const Animation2d &other):numOffsets(other.numOffsets),animationLength(other.animationLength){
    keyFrames = other.keyFrames;
}

Animation2d::Animation2d(const string fileName):Animation2d(){
    load(fileName);
}

Animation2d::~Animation2d(){}


void Animation2d::load(const string filename){
    ifstream file(filename.c_str());
    file>>*this;
    file.close();
}

const KeyFrame2d &Animation2d::getFrame(const size_t frame){
    return keyFrames[frame];
}

size_t Animation2d::numKeyFrames(){
    return keyFrames.size();
}

size_t Animation2d::getSize(){
    return numOffsets;
}

double Animation2d::length(){
    return animationLength;
}

ostream &operator<<(ostream &out, const Animation2d &anim){
    out<<"#ANIMATION2D"<<endl;
    out<<anim.numOffsets<<endl;
    out<<anim.keyFrames.size()<<endl;
    for(size_t x = 0; x < anim.keyFrames.size();x++)
        out<<anim.keyFrames[x]<<endl;
    
    return out;
}

istream &operator>>(istream &in, Animation2d &anim){
    static string headerCheck;
    in>>headerCheck;
    if(headerCheck.compare("#ANIMATION2D") != 0)
        return in;
    in>>anim.numOffsets;
    cout<<"num offsets "<<anim.numOffsets<<endl;
    int numFrames;
    in>>numFrames;
    //resize keyframes to fit new data
    anim.keyFrames.resize(numFrames);
    for(size_t x = 0; x < anim.keyFrames.size(); x++){
        //resize keyframe and read in
        anim.keyFrames[x].setSize(anim.numOffsets);
        in>>anim.keyFrames[x];
    }
    anim.animationLength = anim.keyFrames[anim.keyFrames.size()-1].time;
    return in;
}

ostream &operator<<(ostream &out, const KeyFrame2d &frame){
    out<<"#KEYFRAME2D"<<endl;
    out<<frame.time<<endl;
    //print offsets except last
    //TODO this loop condition is a little unwieldly
    for(size_t x = 0; x < frame.offsets.size() -1 ;x++){
        out<<frame.offsets[x]<<endl;
    }
    //print last offset without trailing newline
    out<<frame.offsets.back();
    return out;
}

istream &operator>>(istream &in, KeyFrame2d &frame){
    static string headerCheck;
    in>>headerCheck;
    if(headerCheck.compare("#KEYFRAME2D") != 0)
        return in;
    in>>frame.time;
    for(size_t x = 0; x < frame.offsets.size(); x++){
        in>>frame.offsets[x];
    }
    return in;
}
