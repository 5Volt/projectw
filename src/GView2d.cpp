#include "GView2d.h"
using namespace std;
using namespace GMath;

GView2d::GView2d(const string name, int glGen, int glVer): GView(name, glGen, glVer)
{
    //ctor
}

GView2d::~GView2d()
{
    //dtor
}

void GView2d::init() {
   
}

void GView2d::draw() {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    objectList::iterator it = objects.begin();

    while(it != objects.end()) {
        (*it)->draw(context);
        ++it;
    }
}

void GView2d::addObject(objRef newObj){
    objects.push_back(newObj);
}

void GView2d::setCamera(const vec2& center, double zoom){
    
    mat3& vpmat = context.VPMatrix;
    vpmat = mat3();
    //NOTE static casts are ugly, but apparently implicit conversions cannot be used in templates
    scale(vpmat, static_cast<mat3::valType>(zoom), static_cast<mat3::valType>(zoom));
    translate(vpmat, center);
    
    mat3& ivpmat = context.IVPMatrix;
    ivpmat = mat3();
    translate(vpmat, center*(vec2::valType)(-1));
    scale(ivpmat, static_cast<mat3::valType>(1.0/zoom),static_cast<mat3::valType>(1.0/zoom));
    
        
}
