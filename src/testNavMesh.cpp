#include<iostream>
#include<sstream>
#include<fstream>
#include<assert.h>
#include<NavMesh.h>
#include<PathFinder.h>

using namespace std;
using namespace GMath;

int main( void ){

    NavMesh mainMesh, mainMesh2;
    
    const string testString("#NAVMESH\n3\n1\n#VERTICES\n( -50 -50 ) ( 0 50 ) ( 50 -50 ) \n#SECTORS\n[0 1 2] {0 0 0} ");
    const string testString2("#NAVMESH\n5\n3\n#VERTICES\n( -50 -50 ) ( 0 50 ) ( 50 -50 ) ( 50 50 ) ( -50 50) \n#SECTORS\n[0 1 4] {1 1 1} [0 1 2] {0 0 2} [1 2 3] {1 1 1} ");
    
    

    
    stringstream mainStream(testString, ios_base::in), mainStream2(testString2, ios_base::in);
    
    mainStream>>mainMesh;
    mainStream2>>mainMesh2;
    
    {
        //test sector retrieval
        cout<<"testing sector detection"<<endl;
        assert(mainMesh.getSector(vec2(0,0)) == 0);
        assert(mainMesh.getSector(vec2(100,100)) != 0);
        
        cout<<"*Test Passed*"<<endl<<endl;
        
    }
    
    {
        //test coarse path finding
        cout<<"testing basic path finding"<<endl;
        SRef<NavMesh::sectorList> testPath =  PathFinder::getCoarsePath(mainMesh, vec2(-49,-49), vec2(0,0));
        assert(testPath->size() == 1);
        assert((*testPath)[0] == 0);
        
        testPath = PathFinder::getCoarsePath(mainMesh2, vec2(-49,49), vec2(49,49));
        
        assert(testPath->size() == 3);
        assert((*testPath)[0] == 0);
        assert((*testPath)[1] == 1);
        assert((*testPath)[2] == 2);
        
        SRef<Path> specPath = PathFinder::refinePath(testPath, mainMesh2, vec2(49,49));
        
        while(!specPath->isEmpty())
            cout<<specPath->popNextPoint()<<' ';
        cout<<endl;
        
        cout<<"*Test Passed*"<<endl<<endl;
        
    }
    cout<<"***All Tests Passed***"<<endl;
    
    return 0;
    
}
