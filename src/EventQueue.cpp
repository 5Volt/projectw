#include "EventQueue.h"

#include<iostream>
using namespace std;

EventQueue::EventQueue()
{
    //ctor
}

EventQueue::~EventQueue()
{
    //dtor
}

void EventQueue::addEvent(const SRef<Event> &newEvent)const{
    events.push(newEvent);
}

const SRef<Event> &EventQueue::nextEvent() const{
    return events.top();
}

SRef<Event> EventQueue::popNextEvent(){
    SRef<Event> retEvent = events.top();
    events.pop();
    return retEvent;
}

bool EventQueue::isEmpty(){
    return events.empty();
}

bool operator<(const SRef<Event> &lhs, const SRef<Event> &rhs){
    //FIXME this causes a crash with signal SIGTRAP
    return lhs->getTime()>rhs->getTime();
    return false;
}
