#include "Animator2d.h"

using namespace std;
using namespace GMath;

Animator2d::Animator2d():currentKeyFrame(),currentAnimationTime(),animation()
{
    lastFrameTime = thisFrameTime = glfwGetTime();
}

Animator2d::Animator2d(SRef<Animation2d> newAnimation):Animator2d(){
    animation = newAnimation;
}

Animator2d::~Animator2d(){}

void Animator2d::update(){
    //TODO Animator2d::update
    if(animation == NULL)
        return;
    
    lastFrameTime = thisFrameTime;
    thisFrameTime = glfwGetTime();
    double dT = thisFrameTime-lastFrameTime;
    currentAnimationTime += dT;
    while(currentAnimationTime > animation->length()){
        currentAnimationTime -= animation->length();
        currentKeyFrame = 0;
    }
}

const mat3 Animator2d::getOffset(size_t index){
    //TODO Animator2d::getOffset
    if(animation == NULL || animation->numKeyFrames() == 0)
        return mat3();
    
    if(currentAnimationTime > animation->getFrame(currentKeyFrame+1).time)
        currentKeyFrame++;
    
    //if up to the last frame
    //NOTE should only really be used in 1 frame animations
    if(currentKeyFrame == animation->numKeyFrames()-1)
        return animation->getFrame(currentKeyFrame).offsets[index];
    
    //currentKeyFrame = the index of the last frame which occurs before the current time
    KeyFrame2d  currFrame = animation->getFrame(currentKeyFrame), 
                nextFrame = animation->getFrame(currentKeyFrame+1);
    
    //calculate factor for interpolation
    // (t - prevFrame)/(nextFrame-prevFrame)
    double interp = static_cast<double>(currentAnimationTime-currFrame.time)/(nextFrame.time-currFrame.time);
    //return linear interpolation of offsets
    return currFrame.offsets[index].interpolate(nextFrame.offsets[index],interp);
}

void Animator2d::setAnimation(SRef<Animation2d> newAnimation){
    currentAnimationTime = 0;currentKeyFrame = 0;
    lastFrameTime = thisFrameTime = glfwGetTime();
    animation = newAnimation;
}
