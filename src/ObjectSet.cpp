#include "ObjectSet.h"

using namespace std;
using namespace GMath;

ObjectSet::ObjectSet():myVect(){}

ObjectSet::~ObjectSet(){}
//iterator functions
ObjectSet::iterator ObjectSet::begin(){
    return myVect.begin();
}

ObjectSet::iterator ObjectSet::end(){
    return myVect.end();
}

//NOTE linear time insertion could be improved
void ObjectSet::insert(const value_type &t){
    myVect.push_back(t);
    updateHash();
}

void ObjectSet::remove(const ObjectSet::iterator unit){
    value_type obj = *unit;
    myVect.erase(unit);
    vec<2,int> pos = obj->getPosition();
    list<value_type> &l = hash[pos.x()][pos.y()];
    auto locInHash = find(l.begin(),l.end(),obj);
    l.erase(locInHash);
}

void ObjectSet::remove(const value_type unit){
    auto it = find(myVect.begin(),myVect.end(), unit);
    if(*it == unit)
        remove(it);
}


SRef<Object> ObjectSet::getObjectAt(const Object *pointer)const{
    for(auto it = myVect.begin(); it != myVect.end(); it++){
        if(*it == pointer)
            return *it;
    }
    return SRef<Object>();
}

ObjectSet::reference ObjectSet::operator[](size_t index){
    return myVect[index];
}

void ObjectSet::updateHash(){
    //TODO update Hash has poor time complexity
    hash.clear();
    for(iterator it = begin(); it != end(); it++){
        vec2 fhashPos = ((*it)->getPosition());
        fhashPos *= 1.0/hashSize;
        vec<2,int> hashPos = fhashPos;
        hash[hashPos.x()][hashPos.y()].push_back(*it);
    }
}

void ObjectSet::setHashSize(double newSize){
    hashSize = newSize;
    updateHash();
}

list<ObjectSet::value_type> ObjectSet::getAdjacent(vec2 position, double adjacency)const{
    position *= 1.0/hashSize;
    vec<2,int> hashPos = position;
    

    list<ObjectSet::value_type> adjacent;
    //the plus one ensures we always round up
    int tiles = adjacency/hashSize + 1;
    for(int x = hashPos.x() - tiles; x < hashPos.x() + tiles; x++){
        for(int y = hashPos.y() - tiles; y < hashPos.y() + tiles; y++){
            if(hash[x][y].size() > 0)
            adjacent.insert(adjacent.end(),hash[x][y].begin(), hash[x][y].end());
        }
    }
    
    return adjacent;
    
}
