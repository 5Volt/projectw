#include "ExplosionEvent.h"

using namespace std;
using namespace GMath;

ExplosionEvent::ExplosionEvent(double size, vec2 epicenter, int damage, double ntime): expSize(size), damage(damage), position(epicenter)
{   
    time = ntime;
}

ExplosionEvent::~ExplosionEvent()
{
    //dtor
}

void ExplosionEvent::execute(GameInformation &info){
    auto inRange = info.units.getAdjacent(position,expSize);
    for(auto it = inRange.begin(); it != inRange.end(); ++it){
        vec2 toTarget = (*it)->getPosition() - position;
        if(toTarget.length() < expSize){
            //damage units which are in range
            SRef<Unit> unitToDamage = *it;
            unitToDamage->applyDamage(damage);
        }
    }
    
}
