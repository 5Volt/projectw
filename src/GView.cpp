#include "GView.h"

using namespace std;

GView::GView(string name, int glGen, int glVer) :running(false) {

    //ctor

    int glmaj,glmin,glrev;
    glfwGetGLVersion(&glmaj,&glmin,&glrev);
    cout<<"computer supports gl version: "<<glmaj<<'.'<<glmin<<" rev "<<glrev<<endl;

    if(glfwInit() == 0)
        cerr<<"failed to initialise GLFW"<<endl;



    //gl hints

    cout<<"initialising openGL v"<<glGen<<'.'<<glVer<<endl;
    glfwOpenWindowHint(GLFW_FSAA_SAMPLES,4);
    glfwOpenWindowHint(GLFW_VERSION_MAJOR,glGen);
    glfwOpenWindowHint(GLFW_VERSION_MINOR,glVer);
    glfwOpenWindowHint(GLFW_OPENGL_PROFILE,GLFW_OPENGL_CORE_PROFILE);

    if(glfwOpenWindow(1024,768,8,8,8,8,8,8,GLFW_WINDOW) != GL_TRUE) {
        cerr<<"failed to open a GLFW window"<<endl;
        glfwTerminate();
    }

    GLenum glew_init_result = glewInit();
    if(glew_init_result != GLEW_OK){
        cerr<<"failed to initialise openGL extensions"<<endl<<"error message: "<<glewGetErrorString(glew_init_result);
        glfwTerminate();
    }

    glfwSetWindowTitle(name.c_str());
    setFrameRate(30);

}

GView::~GView() {
    //dtor
}

void GView::setFrameRate(unsigned int frameRate) {
    period = (1.0/frameRate);
}

void GView::compileShader(GLuint shaderID, istream &shaderSourceStream) {
    string shaderSourceCode;

    // Read the Vertex Shader code from the stream
    if(shaderSourceStream) {
        string Line = "";
        while(getline(shaderSourceStream, Line))
            shaderSourceCode += "\n" + Line;
    }

    GLint Result = GL_FALSE;
    GLsizei InfoLogLength;

    // Compile Vertex Shader
    cout<<"Compiling shader: "<<shaderID<<endl;
    char const * shaderSourcePointer = shaderSourceCode.c_str();
    glShaderSource(shaderID, 1, &shaderSourcePointer , NULL);
    glCompileShader(shaderID);

    // Check Vertex Shader
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);

    if (InfoLogLength == 0) {
        cerr << "Shader " << shaderID << " has no info log" << endl;
    } else {
        vector<char> ShaderErrorMessage(InfoLogLength);
        glGetShaderInfoLog(shaderID, InfoLogLength, NULL, &ShaderErrorMessage[0]);
        cerr<<&ShaderErrorMessage[0]<<endl;
    }

}

void GView::loadShaderPackage(string package) {

    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    string vertex_file_path = package+"/vertex.shd";
    ifstream VertexShaderStream(vertex_file_path.c_str(),ios::in);
    if(!VertexShaderStream.is_open()) {
        return;
    }
    compileShader(VertexShaderID,VertexShaderStream);
    VertexShaderStream.close();

    string fragment_file_path= package +"/fragment.shd";
    ifstream FragmentShaderStream(fragment_file_path.c_str(), ios::in);
    if(!FragmentShaderStream.is_open()) {
        glDeleteShader(VertexShaderID);
        return;
    }
    compileShader(FragmentShaderID,FragmentShaderStream);
    FragmentShaderStream.close();

    // Link the program
    cout<<"Linking program: "<<endl;
    context.shaderID = glCreateProgram();
    glAttachShader(context.shaderID, VertexShaderID);
    glAttachShader(context.shaderID, FragmentShaderID);
    glLinkProgram(context.shaderID);

    context.MVPID = glGetUniformLocation(context.shaderID, "MVP");

    // Check the program
    GLint Result = GL_FALSE;
    GLsizei InfoLogLength;

    glGetProgramiv(context.shaderID, GL_LINK_STATUS, &Result);
    glGetProgramiv(context.shaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    vector<char> ProgramErrorMessage(InfoLogLength);

    if(Result == GL_FALSE) {
        cerr<<"linking shders failed"<<endl;
    }
    if (InfoLogLength == 0) {
        cerr << "Program has no info log" << endl;
    } else {
        glGetProgramInfoLog(context.shaderID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        cout<<&ProgramErrorMessage[0]<<endl;
    }

    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

}

void GView::run() {

    init();

    running = true;
    time = glfwGetTime();
    static double nextFrame = time + period;
    while(running) {
        glUseProgram(context.shaderID);
        //TODO set callback timer instead of chewing up processor time between frames
        while(glfwGetTime()<nextFrame);

        time = nextFrame;
        nextFrame = time + period;
        draw();
        glfwPollEvents();
        glfwSwapBuffers();
        if(glfwGetKey(GLFW_KEY_ESC) == GLFW_PRESS
                || !glfwGetWindowParam( GLFW_OPENED ))
            running = false;
    }

}
