#include "GMesh2d.h"
using namespace std;
using namespace GMath;

GMesh2d::GMesh2d(): GMesh(), vertexArray(), colorArray(){
    drawMode = GL_TRIANGLE_STRIP;
    //ctor
}

GMesh2d::GMesh2d(const string &fileName) {
   load(fileName);
}

GMesh2d::~GMesh2d()
{
    //dtor
}

GMesh2d::GMesh2d(const GMesh2d& other)
{
    //copy ctor
}


void GMesh2d::load(string filename){
    fstream file(filename.c_str());
    
    file>>*this;
    
    file.close();
    
    loadGMem();
    
}

void GMesh2d::loadGMem(){
    //load vertices into GMem
    
    if(!loaded){
        glGenBuffers(1,&meshID);
        glGenBuffers(1,&secondaryID);
    }
    
    glBindBuffer(GL_ARRAY_BUFFER,meshID);
    glBufferData(GL_ARRAY_BUFFER,sizeof(vec2)*numVertices,&vertexArray[0],GL_STATIC_DRAW);
    
    //load colour into GMem
    glBindBuffer(GL_ARRAY_BUFFER,secondaryID);
    glBufferData(GL_ARRAY_BUFFER,sizeof(vec3)*numVertices,&colorArray[0],GL_STATIC_DRAW);
    loaded = true;

}


bool GMesh2d::isLoaded(){
    return loaded;
}

void GMesh2d::draw(){
    
    if(!loaded){
        return;
    }
    //set vertex attributes
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, meshID);
    glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,0,(void*)0);
    //set color attributes
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, secondaryID);
    glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,(void*)0);
    //draw the model
    glDrawArrays(drawMode, 0, numVertices);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
        
}

//stream operators
ostream &operator<<(ostream& out, const GMesh2d& mesh){
    out<<"#MESH2D"<<endl;
    
    out<<mesh.drawMode<<endl;

    out<<"#VERTICES"<<endl;
    
    out<<mesh.numVertices<<endl;
    
    //map vbo data
    
    vec2 nextVec;
    GLsizei x = 0;
    while(x < mesh.numVertices*2){
        nextVec.x() = mesh.vertexArray[x++];
        nextVec.y() = mesh.vertexArray[x++];
        out<<nextVec<<' ';
    }
    out<<endl;
    //load colors
    out<<"#COLOURS"<<endl;
    
    vec3 nextCol;
    x = 0;
    while(x < mesh.numVertices*3){
        nextCol.x() = mesh.colorArray[x++];
        nextCol.y() = mesh.colorArray[x++];
        nextCol.z() = mesh.colorArray[x++];
        out<<nextCol<<' ';
    }
    out<<endl;
    
    return out;
}

istream &operator>>(istream& in, GMesh2d& mesh){
    string headerCheck;
    
    in>>headerCheck;
    static const string expectedHeader("#MESH2D");
    
    if(headerCheck.compare(expectedHeader) != 0)
            return in;

    in>>mesh.drawMode;
    //load vertices
    in>>headerCheck;

    static const string expectedSection("#VERTICES");
    
    if(headerCheck.compare(expectedSection) != 0)
        return in;
    
    in>>mesh.numVertices;
    mesh.vertexArray.clear();
    
    static vec2 nextVec;
    for(GLsizei x = 0; x < mesh.numVertices;x++){
        in>>nextVec;

        mesh.vertexArray.push_back(nextVec.x());
        mesh.vertexArray.push_back(nextVec.y());
    }
    
    //load colors
    in>>headerCheck;
    static const string expectedSection2("#COLOURS");
    
    if(headerCheck.compare(expectedSection2) != 0)
        return in;
        
    mesh.colorArray.clear();
    
    
    static vec3 nextCol;
    for(GLsizei x = 0; x < mesh.numVertices;x++){
        in>>nextCol;
        mesh.colorArray.push_back(nextCol.x());
        mesh.colorArray.push_back(nextCol.y());
        mesh.colorArray.push_back(nextCol.z());
    }

    mesh.loadGMem();
    
    return in;
}
