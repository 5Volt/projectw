var class_animator2d =
[
    [ "Animator2d", "class_animator2d.html#ad56aeca8a23961c816530fb549fe5a4b", null ],
    [ "Animator2d", "class_animator2d.html#aea519e25f6b744d38e99460a72c849ce", null ],
    [ "~Animator2d", "class_animator2d.html#aa00ba2a480ebb1dc402a8ee44cfea399", null ],
    [ "getOffset", "class_animator2d.html#aa920c4d448583b07eca70c73dfc82ff2", null ],
    [ "setAnimation", "class_animator2d.html#ae4f130298559cede7e2160676452fcc2", null ],
    [ "update", "class_animator2d.html#a8eaad25f2757b16a1d489477fadcfe81", null ],
    [ "animation", "class_animator2d.html#af92ebadff27a3dbb15468c7716bfeb19", null ],
    [ "currentAnimationTime", "class_animator2d.html#ac4b6ac3e845605b8842d716f3f025076", null ],
    [ "currentKeyFrame", "class_animator2d.html#a7a67b13d9d02c900082001205cfa6d26", null ],
    [ "lastFrameTime", "class_animator2d.html#a6bedbe1012a4e6871e9d40521441bbb9", null ],
    [ "thisFrameTime", "class_animator2d.html#ab32abddf03ffbd16d72fb62420898b09", null ]
];