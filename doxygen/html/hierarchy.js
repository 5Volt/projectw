var hierarchy =
[
    [ "Animation2d", "class_animation2d.html", [
      [ "EditorAnimation2d", "class_editor_animation2d.html", null ]
    ] ],
    [ "Animator2d", "class_animator2d.html", null ],
    [ "GMath::Bezier::Bezier", "class_g_math_1_1_bezier_1_1_bezier.html", null ],
    [ "ContextInformation", "struct_context_information.html", null ],
    [ "Event", "class_event.html", [
      [ "DeleteEvent", "class_delete_event.html", null ],
      [ "ExplosionEvent", "class_explosion_event.html", null ]
    ] ],
    [ "EventQueue", "class_event_queue.html", null ],
    [ "GameInformation", "struct_game_information.html", null ],
    [ "GMesh", "class_g_mesh.html", [
      [ "GMesh2d", "class_g_mesh2d.html", [
        [ "EditorGMesh2d", "class_editor_g_mesh2d.html", null ]
      ] ]
    ] ],
    [ "GObj2d", "class_g_obj2d.html", [
      [ "EditorGObj2d", "class_editor_g_obj2d.html", null ]
    ] ],
    [ "GView", "class_g_view.html", [
      [ "GView2d", "class_g_view2d.html", [
        [ "Game", "class_game.html", null ],
        [ "MeshEditor", "struct_mesh_editor.html", null ],
        [ "ModelEditor", "class_model_editor.html", null ]
      ] ]
    ] ],
    [ "InputListener", "struct_input_listener.html", [
      [ "Game", "class_game.html", null ],
      [ "MeshEditor", "struct_mesh_editor.html", null ],
      [ "ModelEditor", "class_model_editor.html", null ]
    ] ],
    [ "KeyFrame2d", "struct_key_frame2d.html", null ],
    [ "GMath::mat< width, type >", "struct_g_math_1_1mat.html", null ],
    [ "NavMesh", "class_nav_mesh.html", null ],
    [ "obj", "classobj.html", [
      [ "obj2", "classobj2.html", null ]
    ] ],
    [ "Object", "class_object.html", [
      [ "Unit", "class_unit.html", null ]
    ] ],
    [ "ObjectSet", "class_object_set.html", null ],
    [ "Path", "class_path.html", null ],
    [ "PathFinder", "struct_path_finder.html", null ],
    [ "SObjWrap", "class_s_obj_wrap.html", null ],
    [ "SRef< T >", "class_s_ref.html", null ],
    [ "SRef< Animation2d >", "class_s_ref.html", null ],
    [ "SRef< EditorGMesh2d >", "class_s_ref.html", null ],
    [ "SRef< EditorGObj2d >", "class_s_ref.html", null ],
    [ "SRef< GObj2d >", "class_s_ref.html", null ],
    [ "SRef< Object >", "class_s_ref.html", null ],
    [ "SRef< Path >", "class_s_ref.html", null ],
    [ "SRef< Unit >", "class_s_ref.html", null ],
    [ "GMath::vec< dimensions, type >", "struct_g_math_1_1vec.html", null ],
    [ "GMath::vec< 2 >", "struct_g_math_1_1vec.html", null ]
];