var class_editor_animation2d =
[
    [ "EditorAnimation2d", "class_editor_animation2d.html#a54e996e9d49f226efd2d0cf5be20d48c", null ],
    [ "~EditorAnimation2d", "class_editor_animation2d.html#aec9e5c60b7f0475f91e9209865eb2b70", null ],
    [ "addOffset", "class_editor_animation2d.html#adc758c2da82ed70ffd8135f8751c2473", null ],
    [ "commitKeyFrame", "class_editor_animation2d.html#aa449329cf6703d7406dd56836d360258", null ],
    [ "getKeyFrame", "class_editor_animation2d.html#a2e545bf742f95d411e00ba2f9762a286", null ],
    [ "getOffset", "class_editor_animation2d.html#acfd7a297133f1f26ae5441cedda3ea45", null ],
    [ "removeKeyFrame", "class_editor_animation2d.html#a016c2b1d43e9d0778b6f72f899d0d401", null ],
    [ "removeOffset", "class_editor_animation2d.html#ab67cc27c3d3f4198641055d2f2ee0f13", null ],
    [ "save", "class_editor_animation2d.html#a044a024cd81125322f832930f31a16f1", null ],
    [ "setOffset", "class_editor_animation2d.html#aab340892a2b15c4a282a3f3df8392ac2", null ],
    [ "setSize", "class_editor_animation2d.html#aee84f814e65d00ecb2aeb064125bd041", null ],
    [ "myOffsets", "class_editor_animation2d.html#a4087e696769be2e64b4ab11aa2d388a8", null ]
];