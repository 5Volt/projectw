var class_g_obj =
[
    [ "GObj", "class_g_obj.html#a1b0e062f1db51f19270493f98b3fe424", null ],
    [ "GObj", "class_g_obj.html#aedab4d63847f12659c617afc24ec7458", null ],
    [ "~GObj", "class_g_obj.html#ae8e50ffedac1897cd53253f0e2b390c7", null ],
    [ "draw", "class_g_obj.html#aa03093316880363bf73deae21c0e0172", null ],
    [ "isLoaded", "class_g_obj.html#add671591df0f15a1cb73be81f469a164", null ],
    [ "load", "class_g_obj.html#a1627acaf295072d6b78f5ca5f591f187", null ],
    [ "loadGMem", "class_g_obj.html#a3e0fc4d4fa3163bd7364e37daba7391a", null ],
    [ "save", "class_g_obj.html#a18a327a3f621bbb3029dea5ecd6f1e07", null ],
    [ "drawMode", "class_g_obj.html#ae7402813948e67177eab7fa78cac3827", null ],
    [ "loaded", "class_g_obj.html#a842ffe3b321fcc1bd199e0bce4c2a93f", null ],
    [ "meshID", "class_g_obj.html#a2dc5813d18c1357507a53252fb54caa3", null ],
    [ "numVertices", "class_g_obj.html#a64c9fbc0be3bfb420bd45a79a42efcff", null ],
    [ "secondaryID", "class_g_obj.html#a7696182cb8de0fb51fc1ba9cfc8f6b70", null ]
];