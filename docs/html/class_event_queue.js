var class_event_queue =
[
    [ "EventQueue", "class_event_queue.html#ab7de5a41befc94aac0f461391e67f14a", null ],
    [ "~EventQueue", "class_event_queue.html#ac57db8e2366f2c6c594e6afc975e3b59", null ],
    [ "addEvent", "class_event_queue.html#a388fa3b8ad8d8d4be9b2890700fe2513", null ],
    [ "isEmpty", "class_event_queue.html#aa29d017edc0198d2113018d71d19369c", null ],
    [ "nextEvent", "class_event_queue.html#a81324f14e235a6f9e5b2cc7affe271b3", null ],
    [ "popNextEvent", "class_event_queue.html#a3332ebf7d7d6409dcfa739228299c8b0", null ],
    [ "events", "class_event_queue.html#add7a8fd25d861848e0c0f16e562aada0", null ]
];