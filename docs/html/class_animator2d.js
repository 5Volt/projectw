var class_animator2d =
[
    [ "Animator2d", "class_animator2d.html#ad56aeca8a23961c816530fb549fe5a4b", null ],
    [ "~Animator2d", "class_animator2d.html#aa00ba2a480ebb1dc402a8ee44cfea399", null ],
    [ "getOffset", "class_animator2d.html#a75c2e9e4fdfc5e81fedad9c0368050f3", null ],
    [ "operator<<", "class_animator2d.html#a9d59b339fc9111c6cb8ed513ad91e92b", null ],
    [ "operator>>", "class_animator2d.html#aa4650d38c2c6a61c79e4756830d20f60", null ],
    [ "animations", "class_animator2d.html#a83ff849fcf9a9a3953b7ace134eca65d", null ],
    [ "currentAnimation", "class_animator2d.html#a1092ad42498f66d960e9005008dc7a85", null ],
    [ "lastTime", "class_animator2d.html#a8db61cede7acb1b104856b51b53fea32", null ],
    [ "numOffsets", "class_animator2d.html#ae4402b6dea77def16f7d7f2eacf92355", null ],
    [ "thisTime", "class_animator2d.html#ae7d73a929b6195c3286a0496cab5c039", null ]
];