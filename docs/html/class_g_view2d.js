var class_g_view2d =
[
    [ "objectList", "class_g_view2d.html#aafe74a1e1f2f6f689e9b93b1f114e3c8", null ],
    [ "objRef", "class_g_view2d.html#a8a73c32b6778aa2469f6dadb65d4f9a5", null ],
    [ "GView2d", "class_g_view2d.html#a6b91bbe12d8d9a53375a8217fd1940f6", null ],
    [ "~GView2d", "class_g_view2d.html#a26b1b3fc631639f2dcd2acf26d488678", null ],
    [ "addObject", "class_g_view2d.html#ae09e6b77f0fd75a5da6827b00574736c", null ],
    [ "draw", "class_g_view2d.html#a60d87c2221299a1896e3336e6f3afe25", null ],
    [ "init", "class_g_view2d.html#a45506adbebca8eba7d5730eac54f5553", null ],
    [ "setCamera", "class_g_view2d.html#a5e9afab34740562e9276bad71ba06965", null ],
    [ "objects", "class_g_view2d.html#a3fbdc0a6b3d2c10af946ad6bf98d354f", null ]
];