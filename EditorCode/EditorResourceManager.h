#ifndef EDITORRESOURCEMANAGER_H_INCLUDED
#define EDITORRESOURCEMANAGER_H_INCLUDED

#include <ResourceManager.h>

class EditorGObj2d;
class EditorGMesh2d;
class EditorAnimation2d;


namespace ResourceManager{
namespace Editor{
    //GMesh
    SRef<EditorGMesh2d> getEditableMesh(const std::string &meshID);
    bool meshExists(const std::string &meshID);
    void registerMesh(SRef<EditorGMesh2d> mesh, const std::string &meshID);
    void saveMesh(const std::string &meshID);
    std::string getMeshName(const SRef<GMesh2d> mesh);
    //GObj
    SRef<EditorGObj2d> getEditableObject(const std::string &objID);
    bool objectExists(const std::string &objID);
    void registerObject(SRef<EditorGObj2d> object, const std::string &objID);
    void saveObject(const std::string &objID);
    //Animation
    SRef<EditorAnimation2d> getEditableAnimation(const std::string &animID);
    bool animExists(const std::string &animID);
    void registerAnimation(SRef<EditorAnimation2d> animation, const std::string &animID);
    void saveAnimation(const std::string &animID);
}
}

#endif // EDITORRESOURCEMANAGER_H_INCLUDED
