#include<iostream>
#include<ModelEditor.h>

using namespace std;

int main(void){
    ModelEditor mainEditor;
    mainEditor.loadShaderPackage("shaders/color2dShaderPack");
    mainEditor.setFrameRate(30);
    mainEditor.run();
}
