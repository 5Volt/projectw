#ifndef EDITORGOBJ2D_H
#define EDITORGOBJ2D_H

#include <iostream>

#include <GObj2d.h>
#include <EditorAnimator2d.h>
#include <EditorResourceManager.h>


class EditorGObj2d : public GObj2d
{
    public:
        EditorGObj2d();
        EditorGObj2d(const EditorGObj2d &other);
        EditorGObj2d(const std::string &fileName);

        virtual ~EditorGObj2d();
        
        virtual void save(std::string);

        virtual void addMesh(meshRef newMesh);
        
        virtual void draw(const ContextInformation &context);
        
        virtual void drawChoose( GLuint chooseColorID, const ContextInformation &context);
        virtual meshRef getMesh(size_t index);
        
        EditorAnimator2d &getAnimator();
        
    protected:
        
        EditorAnimator2d editAnimation;
        
    private:
};

#endif // EDITORGOBJ2D_H
