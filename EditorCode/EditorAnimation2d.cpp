#include "EditorAnimation2d.h"

using namespace std;
using namespace GMath;

EditorAnimation2d::EditorAnimation2d():Animation2d()
{
    //ctor
}

EditorAnimation2d::EditorAnimation2d(const EditorAnimation2d &other):Animation2d(other){}
EditorAnimation2d::EditorAnimation2d(const std::string &fileName):Animation2d(fileName){}

EditorAnimation2d::~EditorAnimation2d()
{
    //dtor
}


void EditorAnimation2d::addOffset(mat3 newOffset){
    cout<<"adding offset to animation"<<endl;
    numOffsets++;
    for(auto it = keyFrames.begin(); it!= keyFrames.end();++it){
        cout<<"adding offset to keyframe"<<endl;
        it->setSize(numOffsets);
        it->offsets[numOffsets-1] = newOffset;
    }
}

void EditorAnimation2d::removeOffset(size_t index){
    
    for(size_t x = index; x < numOffsets-1;x++){
        //remove offset from all keyFrames
        for(auto it = keyFrames.begin(); it!= keyFrames.end();++it)
            it->offsets[x] = it->offsets[x+1];
    }
    numOffsets--;
    //resize local offsets
    for(auto it = keyFrames.begin(); it!= keyFrames.end();++it)
        it->setSize(numOffsets);
    
}

void EditorAnimation2d::setSize(size_t newNumOffsets){
    numOffsets = newNumOffsets;
    for(auto it = keyFrames.begin(); it != keyFrames.end(); it++)
        it->setSize(numOffsets);
    cout<<"about to set size to "<<numOffsets<<endl;
}

void EditorAnimation2d::save(const string filename){
    ofstream file(filename.c_str());
    file<<*this;
    file.close();
}

void EditorAnimation2d::addKeyFrame(KeyFrame2d frame){
    //TODO commitKeyFrame
    if(frame.offsets.size() != numOffsets)
        cout<<"butts"<<endl;
        //TODO throw
    auto it = keyFrames.begin();
    while(it != keyFrames.end() && it->time < frame.time)
        it++;
    keyFrames.insert(it,frame);
    cout<<"key frame added "<<frame<<endl;
    cout<<"Animation length is "<<keyFrames.size()<<" frames"<<endl;
    cout<<*this<<endl;
}

void EditorAnimation2d::removeKeyFrame(size_t index){
    for(size_t x = index; x < keyFrames.size(); x++)
        keyFrames[x] = keyFrames[x+1];
    keyFrames.resize(keyFrames.size()-1);
}

KeyFrame2d &EditorAnimation2d::getKeyFrame(size_t index){
    return keyFrames[index];
}
