#ifndef MODELEDITOR_H_INCLUDED
#define MODELEDITOR_H_INCLUDED

#include <iostream>
#include <fstream>

#include"GView2d.h"
#include"MeshEditor.h"
#include"EditorGObj2d.h"
#include <GL/glfw.h>
#include"InputListener.h"
enum editMode{Drag, Rotate, Scale, Delete};

class ModelEditor : public GView2d, public InputListener {

public:
    ModelEditor();

    ~ModelEditor();

    virtual void mousePressed( int button);
    virtual void keyPressed(int key);
    virtual void mouseDragged(int mouseX, int mouseY);

    virtual GMath::vec2 toModelSpace(GMath::vec<2,int> screenPos);

    virtual void save();
    virtual void load();
    virtual void init();
    virtual void draw();

    virtual void addMesh();

    virtual void translateOffset(GMath::vec2 translation);
    virtual void scaleOffset(double scaleFactor);
    virtual void rotateOffset(double rotation);

    virtual void regKeyFrame();
    virtual void saveAnimation();

protected:
    SRef<EditorGObj2d> myObj;
    SRef<EditorGMesh2d> currMesh;
    size_t selectedMeshIndex;

    GLuint chooseShaderID;
    GLuint chooseColorID, chooseMVPID;

    int prevMouseX, prevMouseY;

    editMode mode;

    std::string workingID;
    bool autoSave;

};

#endif // MODELEDITOR_H_INCLUDED
