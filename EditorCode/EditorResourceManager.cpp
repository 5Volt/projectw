#include <EditorResourceManager.h>
#include <EditorGMesh2d.h>
#include <EditorGObj2d.h>


namespace ResourceManager{
using namespace std;


namespace Editor{
    map<string, SRef<EditorGMesh2d> > loadedEditorMeshes;
    map<string, SRef<EditorGObj2d> > loadedEditorObjects;
    map<string, SRef<EditorAnimation2d> > loadedEditorAnimations;
    

    
    SRef<EditorGMesh2d> getEditableMesh(const string &meshID){
        if(loadedEditorMeshes[meshID] == NULL){
            //mesh not yet loaded
            string fullName = meshDirectory;
            fullName.append(meshID);
            fullName.append(meshExtension);
            loadedEditorMeshes[meshID] = new EditorGMesh2d(fullName);
        }
        return loadedEditorMeshes[meshID];
    }
    
    bool meshExists(const string &meshID){
        if(loadedEditorMeshes[meshID] == NULL){
            string fullName = meshDirectory;
            fullName.append(meshID);
            fullName.append(meshExtension);
            ifstream file(fullName.c_str());
            //file closed when destructor called at end of function scope
            return file;
        }
        return true;
    }
    
    void registerMesh(SRef<EditorGMesh2d> mesh, const string &meshID){
        //NOTE this will cause registerMesh to overwrite any existing mesh
        //TODO throw meshExits exception
        loadedEditorMeshes[meshID] = mesh.clone();
    }
    
    void saveMesh(const string &meshID){
        string fullName = meshDirectory;
        fullName.append(meshID);
        fullName.append(meshExtension);
        loadedEditorMeshes[meshID]->save(fullName);
    }
    
    string getMeshName(const SRef<GMesh2d> mesh){
        for(auto it = loadedEditorMeshes.begin(); it != loadedEditorMeshes.end(); it++){
            if(it->second == mesh)
                return it->first;
        }
            
        return "";
    }
    
    //GObj2d
    SRef<EditorGObj2d> getEditableObject(const std::string &objID){
        //TODO getEditableObject
        if(loadedEditorObjects[objID] == NULL){
            cout<<"loading object"<<endl;
            string fullName = objectDirectory;
            fullName.append(objID);
            fullName.append(objectExtension);
            loadedEditorObjects[objID] = new EditorGObj2d(fullName);
        }
        cout<<"object loaded"<<endl;
        return loadedEditorObjects[objID].clone();
    }
    
    bool objectExists(const std::string &objID){
        //TODO objectExists
        if(loadedEditorObjects[objID] == NULL){
            string fullName = objectDirectory;
            fullName.append(objID);
            fullName.append(objectExtension);
            ifstream file(fullName.c_str());
            //file closed when destructor called at end of function scope
            return file;
        }
        return true;
    }
    
    void registerObject(SRef<EditorGObj2d> object, const std::string &objID){
        //TODO registerObject
        loadedEditorObjects[objID] = object.clone();
        cout<<"registering object:"<<endl<<*loadedEditorObjects[objID]<<endl;
    }
    
    void saveObject(const std::string &objID){
        //TODO saveObject
        string fullName = objectDirectory;
        fullName.append(objID);
        fullName.append(objectExtension);
        loadedEditorObjects[objID]->save(fullName);

    }

    //Animation
    SRef<EditorAnimation2d> getEditableAnimation(const std::string &animID){
        if(loadedEditorAnimations[animID] == NULL){
            string fullName = animationDirectory;
            fullName.append(animID);
            fullName.append(animationExtension);
            loadedEditorAnimations[animID] = new EditorAnimation2d(fullName);
        }
        return loadedEditorAnimations[animID];
    }
    
    void registerAnimation(SRef<EditorAnimation2d> animation, const std::string &animID){
        loadedEditorAnimations[animID] = animation.clone();
    }
    
    void saveAnimation(const std::string &animID){
        string fullName = animationDirectory;
        fullName.append(animID);
        fullName.append(animationExtension);
        loadedEditorAnimations[animID]->save(fullName);
    }
}
}
