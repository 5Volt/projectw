#include "EditorGMesh2d.h"

using namespace std;
using namespace GMath;

EditorGMesh2d::EditorGMesh2d():GMesh2d()
{
    //ctor
}

EditorGMesh2d::EditorGMesh2d(const string &file){
    load(file);
}

EditorGMesh2d::~EditorGMesh2d()
{
    //dtor
}

void EditorGMesh2d::addPoint(const vec2 &newPoint, const vec3 &newColor){
    
    numVertices++;
        
    vertexArray.push_back(newPoint.x());
    vertexArray.push_back(newPoint.y());
    

    colorArray.push_back(newColor.x());
    colorArray.push_back(newColor.y());
    colorArray.push_back(newColor.z());
    
    loadGMem();
    
}

void EditorGMesh2d::save(const string &filename){
    if(!loaded) return;
    
    ofstream file(filename.c_str());
    file<<*this;
    file.close();
}
