#include "EditorAnimator2d.h"

using namespace std;
using namespace GMath;

EditorAnimator2d::EditorAnimator2d():Animator2d()
{
    //ctor
}

EditorAnimator2d::~EditorAnimator2d()
{
    //dtor
}

void EditorAnimator2d::setOffset(size_t index, mat3 newOffset){
    myOffsets[index] = newOffset;
}

void EditorAnimator2d::commitKeyFrame(double time){
    KeyFrame2d newFrame;
    newFrame.offsets.push_back(myOffsets[0]);
    newFrame.time = time;
    cout<<"new frame created"<<endl<<newFrame<<endl;
    SRef<EditorAnimation2d> anim = animation;
    anim->addKeyFrame(newFrame);
}
        
const mat3 EditorAnimator2d::getOffset(size_t index){
    return myOffsets[index];
}
void EditorAnimator2d::addOffset(mat3 newOffset){
    cout<<"adding offset to animator"<<endl;
    myOffsets.push_back(newOffset);
    SRef<EditorAnimation2d> anim = animation;
    anim->addOffset(newOffset);
}
void EditorAnimator2d::setSize(size_t newSize){
    SRef<EditorAnimation2d> anim = animation;
    anim->setSize(newSize);
    myOffsets.resize(newSize);
}

void EditorAnimator2d::setAnimation(SRef<EditorAnimation2d> newAnimation){
    animation = newAnimation;
    SRef<EditorAnimation2d> anim = animation;
    if(animation->getSize() != myOffsets.size())
        anim->setSize(myOffsets.size()); //TODO throw
    else 
        cout<<"all things go"<<endl;
    cout<<"animation size "<<animation->getSize()<<endl;
}
SRef<EditorAnimation2d> EditorAnimator2d::getAnimation(){
    return animation;
}
