#include "EditorGObj2d.h"

using namespace std;
using namespace GMath;

EditorGObj2d::EditorGObj2d():GObj2d()
{
    editAnimation.setAnimation(new EditorAnimation2d());
    editAnimation.setSize(meshes.size());
}

EditorGObj2d::~EditorGObj2d()
{
    //dtor
}
        
EditorGObj2d::EditorGObj2d(const EditorGObj2d &other):GObj2d(other),editAnimation(other.editAnimation){
    //TODO editorGObj copystructor
}

EditorGObj2d::EditorGObj2d(const std::string &fileName): GObj2d(), editAnimation(){
    load(fileName);
    editAnimation.setAnimation(new EditorAnimation2d());
    editAnimation.setSize(meshes.size());
}
        
void EditorGObj2d::save(string filename){
    ofstream file(filename.c_str());
    
    file<<*this;
    file.close();
    
}


void EditorGObj2d::addMesh(meshRef newMesh){
    
    numMeshes++;
    meshNames.push_back(ResourceManager::Editor::getMeshName(newMesh));
    meshes.push_back(newMesh);
    
    editAnimation.addOffset();
    
}

void EditorGObj2d::draw(const ContextInformation &context){
        
    mat3 MVPMatrix = context.VPMatrix * modelMatrix;
    for(size_t x = 0; x < numMeshes; x++){
        mat3 LMVPMatrix = MVPMatrix * editAnimation.getOffset(x);
        glUniformMatrix3fv(context.MVPID,9, GL_FALSE,LMVPMatrix.toArray());
        meshes[x]->draw();
    }
    
}

void EditorGObj2d::drawChoose( GLuint chooseColorID, const ContextInformation &context){
    //TODO drawChoose
    mat3 MVPMatrix = context.VPMatrix * modelMatrix;
    
    for(size_t x = 0; x < numMeshes; x++){
        mat3 LMVPMatrix = MVPMatrix * editAnimation.getOffset(x);
        vec3 col;
        col[0] = static_cast<float>(x)/256.0f;
        glUniform3fv(chooseColorID,3,&col[0]);
        glUniformMatrix3fv(context.MVPID,9, GL_FALSE,LMVPMatrix.toArray());
        meshes[x]->draw();
    }
    
}

EditorGObj2d::meshRef EditorGObj2d::getMesh(size_t index){
    return meshes[index];
}

EditorAnimator2d &EditorGObj2d::getAnimator(){
    return editAnimation;
}
