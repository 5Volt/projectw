#include <ModelEditor.h>

using namespace std;
using namespace GMath;


ModelEditor::ModelEditor():GView2d("Editor",3,2),myObj(), currMesh(),selectedMeshIndex(),mode(Drag){
    loadShaderPackage("shaders/choose2dShaderPack");
    chooseColorID = glGetUniformLocation(context.shaderID, "choose_color");
    chooseMVPID = glGetUniformLocation(context.shaderID, "MVP");
    chooseShaderID = context.shaderID;
}

ModelEditor::~ModelEditor(){}

void ModelEditor::mousePressed(int button){
    //TODO mousePressed
    
    prevMouseX = mouseX;
    prevMouseY = mouseY;
    GLuint  originalMVPID = context.MVPID;
            
    context.MVPID = chooseMVPID;
    
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(chooseShaderID);
    
    myObj->drawChoose(chooseColorID, context);
    
    //restore mvpid
    context.MVPID = originalMVPID;
    //retreive draw information from the frame buffer
    unsigned char pixel[3];
    
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    glReadPixels(mouseX, viewport[3] - mouseY, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);
    
    selectedMeshIndex = pixel[0];
    cout<<"selected mesh "<<selectedMeshIndex<<endl;
}

void ModelEditor::mouseDragged(int mouseX, int mouseY){
    vec2 diff;
    double factor;
    mat3 offset = myObj->getAnimator().getOffset(selectedMeshIndex);
    switch(mode){
    case Drag:
        diff = toModelSpace(vec<2,int>(mouseX, mouseY)) - toModelSpace(vec<2,int>(prevMouseX,prevMouseY));
        translateOffset(diff);
        break;
    case Scale:
        factor = (vec2((vec3(mouseX,mouseY,1)*context.IVPMatrix)).length()/vec2((vec3(prevMouseX,prevMouseY,1)*context.IVPMatrix)).length());
        scaleOffset(factor);
        break;
    case Rotate:
        factor = -(azimuth(vec2((vec3(mouseX,mouseY,1)*context.IVPMatrix)))-azimuth(vec2((vec3(prevMouseX,prevMouseY,1)*context.IVPMatrix))));
        rotateOffset(factor);
        break;
    }
    
    prevMouseX = mouseX;
    prevMouseY = mouseY;

}

void ModelEditor::translateOffset(vec2 translation){
    EditorAnimator2d &anim = myObj->getAnimator();
    
    mat3 offset = anim.getOffset(selectedMeshIndex);
    translate(offset,translation);
        
    anim.setOffset(selectedMeshIndex,offset);
}

void ModelEditor::scaleOffset(double scaleFactor){
    EditorAnimator2d &anim = myObj->getAnimator();
    
    mat3 offset = anim.getOffset(selectedMeshIndex);
    scale(offset, scaleFactor);
    
    anim.setOffset(selectedMeshIndex,offset);
}

void ModelEditor::rotateOffset(double rotation){
    EditorAnimator2d &anim = myObj->getAnimator();
    
    mat3 offset = anim.getOffset(selectedMeshIndex);
    rotate(offset, rotation);
    
    anim.setOffset(selectedMeshIndex,offset);
}

void ModelEditor::keyPressed(int key){
    switch(key){
    case 'S':
        save();
        break;
    case 'L':
        load();
        break;
    case 'A':
        addMesh();
        break;
    case 'K':
        regKeyFrame();
        break;
    case 'G':
        saveAnimation();
        break;
    case 'Z':
        mode = Drag;
        break;
    case 'X':
        mode = Delete;
        break;
    case 'C':
        mode = Scale;
        break;
    case 'V':
        mode = Rotate;
        break;
    }
}

vec2 ModelEditor::toModelSpace(vec<2,int> screenPos){
 //FIXME only works for camera at (0,0)
    int width,height;
    glfwGetWindowSize(&width,&height);
    vec2 newPoint;
    if(mouseX > width/2)
        newPoint.x() = screenPos.x()-width/2;
    else
        newPoint.x() = -(width/2 - screenPos.x());
    
    if(mouseY > height/2)
        newPoint.y() = -(screenPos.y()-height/2);
    else
        newPoint.y() = height/2 - screenPos.y();
    
    newPoint.x() *= (static_cast<vec2::valType>(1)/(width/2));        
    newPoint.y() *= (static_cast<vec2::valType>(1)/(height/2));
    
    newPoint = vec3(newPoint.x(), newPoint.y(), 1) * context.IVPMatrix;
    
    return newPoint;
}

void ModelEditor::save(){
    if(!autoSave){
        char confirmChar;
        cout<<"Are you sure you wish to save over the current file?"<<endl<<"(y/n)"<<endl;
        cin>>confirmChar;
        if(confirmChar == 'y' || confirmChar == 'Y')
            autoSave = true;
        else
            return;
    }
    ResourceManager::Editor::registerObject(myObj, workingID);
    cout<<"saving"<<endl<<*ResourceManager::Editor::getEditableObject(workingID)<<endl;
    ResourceManager::Editor::saveObject(workingID);
}

void ModelEditor::load(){
    cout<<"please enter the ID of the new model to load"<<endl;
    cin>>workingID;
    currMesh = ResourceManager::Editor::getEditableObject(workingID);
    autoSave = false;
}

void ModelEditor::init(){
    workingID = "editorModel";
    myObj = ResourceManager::Editor::getEditableObject(workingID);
    myObj->getAnimator().setAnimation(new EditorAnimation2d());
    autoSave = false;
}

void ModelEditor::draw(){
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(context.shaderID);

    myObj->draw(context);
}

void ModelEditor::regKeyFrame(){
    EditorAnimator2d &anim = myObj->getAnimator();
    cout<<"enter the time for this keyframe"<<endl;
    double kTime;
    cin>>kTime;
    anim.commitKeyFrame(kTime);
    cout<<"key frame committed"<<endl;
    cout<<*anim.getAnimation()<<endl;
}

void ModelEditor::saveAnimation(){
    cout<<"please enter name to save animation under"<<endl;
    string animName;
    cin>>animName;
    SRef<EditorAnimation2d> anim = myObj->getAnimator().getAnimation();

    cout<<"saving animation: "<<endl<<*myObj->getAnimator().getAnimation()<<endl;
    ResourceManager::Editor::registerAnimation(anim,animName);
    ResourceManager::Editor::saveAnimation(animName);
}

void ModelEditor::addMesh(){
    cout<<"please enter the name of the mesh you wish to add"<<endl;
    string meshID;
    cin>>meshID;
    currMesh = ResourceManager::Editor::getEditableMesh(meshID);
    myObj->addMesh(currMesh);
}
