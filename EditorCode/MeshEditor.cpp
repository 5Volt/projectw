#include <MeshEditor.h>

using namespace std;
using namespace GMath;


// implement constructors
MeshEditor::MeshEditor():GView2d("Editor",3,2), InputListener(){
}

MeshEditor::~MeshEditor(){
    save();
}
//utility function to empty in stream
void MeshEditor::empty(std::istream &in){
//        char dummy;
//        in.rdbuf()->sputc(EOF);
//        while(!in.eof())in>>dummy;
}

SRef<EditorGMesh2d> currMesh;
GMath::vec3 col;

void MeshEditor::keyPressed(int key){
    
    switch(key){
    case 'L':
        load();
        break;
    case 'S':
        save();
        break;
    case 'A':
        cout<<"please enter a new ID to save as"<<endl;
        empty(cin);
        cin>>workingID;
        if(!ResourceManager::Editor::meshExists(workingID)){
            autoSave = true;
        }else{
            autoSave = false;
            ResourceManager::Editor::registerMesh(currMesh,workingID);
        }
        save();
        break;
    case 'C':
        cout<<"please enter a new color in RGB format"<<endl;
        empty(cin);
        cin>>col;
        break;
    }
}

int gridSize;

void MeshEditor::mousePressed(int button){
        
    int width,height;
    glfwGetWindowSize(&width,&height);
    vec2 newPoint;
    int gridX(mouseX-(mouseX%gridSize) +gridSize/2),gridY(mouseY-(mouseY%gridSize)+gridSize/2);
    if(mouseX > width/2)
        newPoint.x() = gridX-width/2;
    else
        newPoint.x() = -(width/2 - gridX);
    
    if(mouseY > height/2)
        newPoint.y() = -(gridY-height/2);
    else
        newPoint.y() = height/2 - gridY;
    
    newPoint.x() *= (static_cast<vec2::valType>(1)/(width/2));        
    newPoint.y() *= (static_cast<vec2::valType>(1)/(height/2));
    
    currMesh->addPoint(newPoint,col);
    
}

string workingID;
bool autoSave;

void MeshEditor::save(){
    if(autoSave == false){
        char confirmChar;
        empty(cin);
        cout<<"are you sure you want to save over the previous file?"<<endl<<"(enter 'y' to confirm)"<<endl;
        cin>>confirmChar;
        switch(confirmChar){
            case 'Y':
            case 'y':
                autoSave = true;
            default:
                return;
        }
    }
    ResourceManager::Editor::saveMesh(workingID);
    cout<<"mesh saved successfully"<<endl;
}

void MeshEditor::load(){
    cout<<"enter MeshID to load"<<endl;
    cin>>workingID;
    //test if the file exists
    currMesh = ResourceManager::Editor::getEditableMesh(workingID);
    if(currMesh != NULL){
        autoSave = false;
    } else {
        currMesh = new EditorGMesh2d();
        autoSave = true;
    }
}


void MeshEditor::init(){
    SetFocus();
    workingID = "editorMesh";
    currMesh = new EditorGMesh2d();
    gridSize = 10;
    col.x() = col.y() = col.z() = 1;
    if(!ResourceManager::Editor::meshExists(workingID)){
        autoSave = true;
    } else {
        currMesh = ResourceManager::Editor::getEditableMesh(workingID);
    }
}

void MeshEditor::draw(){
    glUseProgram(context.shaderID);
    mat3 MVPMatrix = context.VPMatrix;
    glUniformMatrix3fv(context.MVPID,9, GL_FALSE,MVPMatrix.toArray());
    glClear(GL_COLOR_BUFFER_BIT);
    currMesh->draw();
}


