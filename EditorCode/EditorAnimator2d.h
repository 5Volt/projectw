#ifndef EDITORANIMATOR2D_H
#define EDITORANIMATOR2D_H

#include <EditorAnimation2d.h>
#include <Animator2d.h>
#include <SRef.h>
#include <GMath.h>



class EditorAnimator2d : public Animator2d
{
    public:
        EditorAnimator2d();
        virtual ~EditorAnimator2d();
        
        /**
         * Sets an offset in the current frame to a new desired value
         * @param   index   The index of the offset to be changed
         * @param   newOffset   The new value for the offset
         **/
        virtual void setOffset(size_t index, GMath::mat3 newOffset);
        
        /**
         * Creates a new keyFrame with offset values equal to the current myOffsets vector
         * @param   time    The time at which a new keyFrame should be created
         **/
        virtual void commitKeyFrame(double time);
        //TODO document
        virtual const GMath::mat3 getOffset(size_t index);
        
        virtual void addOffset(GMath::mat3 newOffset = GMath::mat3());
        virtual void setSize(size_t newSize);
        
        //name hiding should cause the non editor animation's setAnimation function to be invisible
        virtual void setAnimation(SRef<EditorAnimation2d> newAnimation);
        
        virtual SRef<EditorAnimation2d> getAnimation();

    protected:
        std::vector<GMath::mat3> myOffsets;

    private:
};

#endif // EDITORANIMATOR2D_H
