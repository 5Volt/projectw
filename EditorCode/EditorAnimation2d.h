#ifndef EDITORANIMATION2D_H
#define EDITORANIMATION2D_H

#include <Animation2d.h>


class EditorAnimation2d : public Animation2d
{
    public:
        EditorAnimation2d();
        EditorAnimation2d(const EditorAnimation2d &other);
        EditorAnimation2d(const std::string &fileName);
        virtual ~EditorAnimation2d();
        
        /**
         * Increases the number of offsets which this animation supports
         * @param   newOffset   The offset to be put in the new position
         * for all keyframes in this animation
         **/
        virtual void addOffset(GMath::mat3 newOffset = GMath::mat3());
        
        /**
         * Removes and offset and decreases the number of offsets by one
         * @param   index   The index of the offset which is to be removed
         **/
        virtual void removeOffset(size_t index);
        
        /**
         * Sets the number of offsets which this animation will support.
         * @param   numOffsets   The number of offsets this animation must support
         **/
        virtual void setSize(size_t numOffsets);
        
        /**
         * Saves data into a file in the file system for later retrieval
         * If the file specified already exists, it will be replaced, otherwise a new file is created
         * @param   filename    The path of the file to be saved to
         **/
        virtual void save(const std::string filename);
        
        /**
         * Adds a new keyFrame to this animation
         * @param   newFrame    The new frame to be added
         **/
        virtual void addKeyFrame(KeyFrame2d newFrame);
        
        /**
         * Removes a keyFrame from this animation
         * @param   index   Which keyFrame to remove
         **/
        virtual void removeKeyFrame(size_t index);
        
        /**
         * gets a keyFrame from this animation for inspection
         * @param   index   Which keyFrame to retrieve
         **/
        virtual KeyFrame2d &getKeyFrame(size_t index);
    protected:
    private:
};

#endif // EDITORANIMATION2D_H
