#ifndef MESHEDITOR_H_INCLUDED
#define MESHEDITOR_H_INCLUDED

#include <iostream>
#include <fstream>

#include"GView2d.h"
#include"GMath.h"
#include"EditorGMesh2d.h"
#include"InputListener.h"
#include"EditorResourceManager.h"

#include <GL/glfw.h>


struct MeshEditor : public GView2d, public InputListener{
    // implement constructors
    MeshEditor();

    ~MeshEditor();

    //utility function to empty in stream
    void empty(std::istream &in);

    SRef<EditorGMesh2d> currMesh;
    GMath::vec3 col;

    virtual void keyPressed(int key);

    int gridSize;

    virtual void mousePressed(int button);

    std::string workingID;
    bool autoSave;

    virtual void save();
    virtual void load();
    virtual void init();
    virtual void draw();
};

#endif // MESHEDITOR_H_INCLUDED
