#include<iostream>

#include<MeshEditor.h>

using namespace std;

int main(){
    MeshEditor mainEditor;
    
    mainEditor.loadShaderPackage("shaders/color2dShaderPack");
        
    mainEditor.setFrameRate(30);
        
    mainEditor.run();
}
