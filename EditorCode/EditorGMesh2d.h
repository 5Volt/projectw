#ifndef EDITORGMESH2D_H
#define EDITORGMESH2D_H

#include <GMesh2d.h>

class EditorGMesh2d : public GMesh2d
{
    public:
        EditorGMesh2d();
        EditorGMesh2d(const std::string &file);
        virtual ~EditorGMesh2d();
    /**
     * saves the mesh to a file in the file system
     * @param   filename    The path of the file to which this mesh will be saved
     **/      
    virtual void save(const std::string &filename);
        
    /**
     * Adds a point to the data of this GMesh2d after all the points which are already contained.
     * @param   pos The position of the new point to be added
     * @param   col The colour of the new point to be added
     **/
    void addPoint(const GMath::vec2& pos, const GMath::vec3& col);

    protected:
    private:
};

#endif // EDITORGMESH2D_H
